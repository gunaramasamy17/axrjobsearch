package com.axr.jobsearch.utils;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobFilterCriteria {

	private int userId;
	private String list;
	private String date;
	private List<String> startTimeList;
	private List<String> endTimeList;
	private List<String> filterByTime;
	private List<Integer> jobTypeList;
	private List<String> jobStatusList;
	private List<Integer> prefectureList;
	private List<Integer> benefitsList;
	private List<Integer> minSalary;
	private List<Integer> maxSalary;
	private List<String> filterBySalaryRange;
	private HashMap<String,List<DynamicFilter>> jobTimeMap;
	private HashMap<String,List<DynamicFilter>> salaryMap;
	private int sortBy;

	
}
