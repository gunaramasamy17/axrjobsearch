package com.axr.jobsearch.utils;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axr.jobsearch.service.UtilityService;


public class ConfigMap {

	static final Logger logger = LoggerFactory.getLogger(ConfigMap.class);
	
	private static ConfigMap INSTANCE = new ConfigMap();
	
	
	private HashMap<String, HashMap<String,String>> configCategoryMap;

	private ConfigMap() {
		
	}

	public static ConfigMap getInstance() {

		return INSTANCE;
	}

	

	public HashMap<String, HashMap<String,String>> getConfigCategoryMap() {
		return configCategoryMap;
	}

	public void setConfigCategoryMap(HashMap<String, HashMap<String,String>> configCategoryMap) {
		this.configCategoryMap = configCategoryMap;
	}

			

	
}
