package com.axr.jobsearch.utils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axr.jobsearch.dataaccess.model.email.EmailAttribute;
import com.axr.jobsearch.dataaccess.model.email.EmailMessage;
import com.axr.jobsearch.dataaccess.model.email.EmailTemplate;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.service.EmailService;
import com.axr.jobsearch.service.UtilityService;


public class EmailUtility {

	
	private static EmailUtility INSTANCE = new EmailUtility();
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	UtilityService utilityService;
	
	private EmailUtility() {
		
	}

	public static EmailUtility getInstance() {
		return INSTANCE;
	}

	
	public void sendUserInviteEmail(UserProfile userProfile) {
		
		EmailMessage emailMessage = new EmailMessage();
		
		//HashMap<String,HashMap<String,String>> configMap = utilityService.getConfigParameters();
		
		emailMessage.setEmailFrom("axrjobp05t@gmail.com");
		emailMessage.setEmailTo(userProfile.getEmailAddress());
		emailMessage.setEmailBcc("");
		emailMessage.setEmailCC("");
		//TODO: Use Constants or ConfigMap param to get subject value &language 
		emailMessage.setSubject("AXR Platform | Verification Email");
		emailMessage.setContentType("text/html");
		emailMessage.setCharset("UTF-8");
		emailMessage.setStatus("");
		emailMessage.setRetryCount(0);
		emailMessage.setRemarks("");
		emailMessage.setEmailReplyTo("axrjobp05t@gmail.com");
		emailMessage.setCreatedDate(Instant.now());
		emailMessage.setLastUpdatedDate(Instant.now());
		EmailTemplate emailTemplate = emailService.getEmailTemplate("INVITE_USER");
		emailMessage.setEmailTemplate(emailTemplate);
		
		ArrayList<EmailAttribute> emailAttributeList = new ArrayList<EmailAttribute>();
		
		EmailAttribute emailAttribute = new EmailAttribute();
		emailAttribute.setAttributeName("FIRST_NAME");
		emailAttribute.setAttributeValue(userProfile.getFirstName());		
		emailAttributeList.add(emailAttribute);
		
		emailAttribute = new EmailAttribute();
		emailAttribute.setAttributeName("LAST_NAME");
		emailAttribute.setAttributeValue(userProfile.getLastName());
		emailAttributeList.add(emailAttribute);
		
		emailAttribute = new EmailAttribute();
		emailAttribute.setAttributeName("ACTIVATION_LINK");		
		emailAttribute.setAttributeValue("http://localhost/activate/code=123");
		emailAttributeList.add(emailAttribute);
		
		emailMessage.setEmailAttributeList(emailAttributeList);
		
		emailService.sendEmail(emailMessage);
		
		
	}
			

	
}
