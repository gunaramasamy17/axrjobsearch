package com.axr.jobsearch.utils;

import java.util.Arrays;

import com.axr.jobsearch.common.CommonResponse;

public class ResponseUtility {

	public static <T> CommonResponse<T> getResponse(String statusCode, T data, String message) {
		CommonResponse<T> commonResponse = new CommonResponse<T>();
		commonResponse.setStatusCode(statusCode);
		commonResponse.setMessage(message);
		commonResponse.setData(data);
		return commonResponse;
	}
}
