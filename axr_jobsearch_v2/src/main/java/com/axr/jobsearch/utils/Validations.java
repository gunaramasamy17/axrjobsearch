package com.axr.jobsearch.utils;

import com.axr.jobsearch.common.Constants;

public class Validations {

	public static String validatePassword(String password, String confirmPassword) {
		String message;
		if (null != password
				&& !password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$") &&  null != confirmPassword && !confirmPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
			{
			 message ="強力なパスワードを入力してください (最小8、1つの大文字、1つの小文字、1つの数値、1つの特殊文字)";
			return message;
			}
			 
			}else if(!password.equals(confirmPassword))
				{
					message = "パスワードと同じ値を入力してください";
					return message;
				}
			else {
				return Constants.SUCCESS;
			}
	}
}

