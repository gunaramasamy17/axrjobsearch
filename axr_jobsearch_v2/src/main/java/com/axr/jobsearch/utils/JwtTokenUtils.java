package com.axr.jobsearch.utils;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.axr.jobsearch.common.Constants;


@Component
public class JwtTokenUtils {

	static final Logger logger = LoggerFactory.getLogger(JwtTokenUtils.class);

	
	public JSONObject getPayload(String token) {
		token.split("\\.");
		String[] jwtParts = token.split("\\.");
		byte[] data = Base64.getDecoder().decode(jwtParts[1]);
		String payload;
		JSONObject jsonObject = null;
		
		try {
			payload = new String(data,Constants.UTF8);
			jsonObject = new JSONObject(payload);
		}catch(UnsupportedEncodingException | JSONException ex) {
			logger.error("Error while parsing json token", ex);
		}
		
		return jsonObject;
	}
}
