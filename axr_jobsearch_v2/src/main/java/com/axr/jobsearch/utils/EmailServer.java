package com.axr.jobsearch.utils;

import java.util.HashMap;
import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.axr.jobsearch.common.Constants;

public class EmailServer {
	
	static final Logger logger = LoggerFactory.getLogger(EmailServer.class);
	
	Session session;
	Transport transport;
	
	private static EmailServer INSTANCE = new EmailServer();
	private static boolean initFlag = false;
	
//	@Value("${mail.smtp.auth}")
	private String smtpAuth;
	
	//@Value("${mail.smtp.starttls.enable}")
	private String tlsEnable;

	//@Value("${mail.smtp.host}")
	private String host;

	//@Value("${mail.transport.protocol}")
	private String protocol;
	
	//@Value("${mail.smtp.port}")
	private String port;
	
	//@Value("${mail.smtp.socketFactory}")
	private String socketFactory;
	
	//@Value("${mail.user.name}")
	private String username;
	
	//@Value("${mail.user.password}")
	private String password;
	
	private EmailServer() {
		
		/* 
		 HashMap<String,String> emailConfigMap = ConfigMap.getInstance().getConfigCategoryMap().get(Constants.EMAIL);
		 Properties props = new Properties(); props.put("mail.smtp.auth",emailConfigMap.get("mail.smtp.auth"));
		 props.put("mail.smtp.starttls.enable",emailConfigMap.get("mail.smtp.starttls.enable"));
		 props.put("mail.smtp.host",emailConfigMap.get("mail.smtp.host"));
		 props.put("mail.transport.protocol", emailConfigMap.get("mail.transport.protocol"));
		 props.put("mail.smtp.port",emailConfigMap.get("mail.smtp.port"));
		 props.put("mail.smtp.socketFactory.class", emailConfigMap.get("mail.smtp.socketFactory.class"));
		logger.error("\n\n****************************************************\n"+props.toString()+"\n\n");
		*/
		
		 Properties props = new Properties(); props.put("mail.smtp.auth","true");
		 props.put("mail.smtp.starttls.enable","true");
		 props.put("mail.smtp.host","smtp.gmail.com");
		 props.put("mail.transport.protocol", "smtp");
		 props.put("mail.smtp.port","465");
		 props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		
		 this.session = Session.getDefaultInstance(props, new javax.mail.Authenticator() { 
			 		protected PasswordAuthentication getPasswordAuthentication() { 
			 			//return new PasswordAuthentication("nextconnect.monitoring@gmail.com","N3x7C0nn3c7"); 
			 			return new PasswordAuthentication("axrjobp05t@gmail.com","AxrJ0bP057"); 
			 		}
			 	});
		 
		 try { 
			 this.transport = session.getTransport(); 
		 }catch(Exception ex) {
			 logger.error("Error while creating Email Transport object",ex); 
	     }
		 
	}
	
	public static EmailServer getInstance() {
		if(!initFlag) INSTANCE.init();
		return INSTANCE; 
	}

	
	  private void init() { 
		  
		  HashMap<String,String> emailConfigMap = ConfigMap.getInstance().getConfigCategoryMap().get(Constants.EMAIL);
		  
	  	  Properties props = new Properties();
		  props.put("mail.smtp.auth",emailConfigMap.get("mail.smtp.auth"));
		  props.put("mail.smtp.starttls.enable",emailConfigMap.get("mail.smtp.starttls.enable"));
		  props.put("mail.smtp.host",emailConfigMap.get("mail.smtp.host"));
		  props.put("mail.transport.protocol", emailConfigMap.get("mail.transport.protocol"));
		  props.put("mail.smtp.port",emailConfigMap.get("mail.smtp.port"));
		  props.put("mail.smtp.socketFactory.class", emailConfigMap.get("mail.smtp.socketFactory.class"));
		  
		  
		  session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
					  protected PasswordAuthentication getPasswordAuthentication() { 
						  return new PasswordAuthentication(emailConfigMap.get("mail.smtp.user.name"),  emailConfigMap.get("mail.smtp.user.password")); 
					  } });
					  
		  try { 
			  transport = session.getTransport(); 
		  }catch(Exception ex) {
			  logger.error("Error while creating Email Transport object",ex); 
		  } 
		  initFlag = true;
	  }
	 	
	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Transport getTransport() {
		return transport;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}
	
	

}
