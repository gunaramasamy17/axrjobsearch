package com.axr.jobsearch.controller;


import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.axr.jobsearch.common.CommonResponse;
import com.axr.jobsearch.common.exception.LoginAuthenticationException;
import com.axr.jobsearch.dataaccess.Country;
import com.axr.jobsearch.dataaccess.model.users.AuthToken;
import com.axr.jobsearch.dataaccess.model.users.Login;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.SkillDetails;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.users.valueobjects.UserProfileVO;
import com.axr.jobsearch.security.AuthUserDetails;
import com.axr.jobsearch.service.SkillService;
import com.axr.jobsearch.service.UserService;
import com.axr.jobsearch.service.users.AuthenticationService;
import com.axr.jobsearch.utils.EmailUtility;
import com.axr.jobsearch.utils.ResponseUtility;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RestController	
public class SkillController {
	
	static final Logger logger = LoggerFactory.getLogger(SkillController.class);
	
	@Autowired
	SkillService skillService;
	
	
		
	@ApiOperation(value = "skills", tags = "Skills", notes = "This API is used to retrieve all the skills")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/skills", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> getSkillDetails() throws AuthenticationException {

		CommonResponse<List<SkillDetails>> response = null; 
		List<SkillDetails> skillDetailsList = null;

		try {
			
			skillDetailsList = skillService.retriveAllSkills();
			response = ResponseUtility.getResponse("200",skillDetailsList, "");
		}catch(Exception ex) {
			logger.error("Error while retriving all the skill details" , ex);
			response = ResponseUtility.getResponse("10501", skillDetailsList, "Error while retrieving all the skill details");
		}

		return new ResponseEntity<CommonResponse>(response,HttpStatus.CREATED);			

	}
	
	@ApiOperation(value = "Get skills", tags = "Skills", notes = "This API is used to retrieve the skills for particular user")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/users/{userId}/skills", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> getUserSkillDetails(
			@ApiParam(name = "userId", value = "User id to get skills") @PathVariable(value="userId") int userId) throws AuthenticationException {

		CommonResponse<List<SkillDetails>> response = null; 
		List<SkillDetails> skillDetailsList = null;
		
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(authUserDetails.getUserId() == userId) {
			try {				
				skillDetailsList = skillService.retriveSkills(userId);
				response = ResponseUtility.getResponse("200",skillDetailsList, "");
			}catch(Exception ex) {
				logger.error("Error while retrieving the skill details for userId->"+userId+"<-" , ex);
				response = ResponseUtility.getResponse("10501", skillDetailsList, "Error while retrieving the skill details for userId: "+userId);
			}
		}else {
			throw new AccessDeniedException("You don't have permission to request for given user id");
		}

		return new ResponseEntity<CommonResponse>(response,HttpStatus.CREATED);			

	}
	
	@ApiOperation(value = "Update Skills", tags = "Skills", notes = "This API is used to update the skills for particular user")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/users/{userId}/skills", method = RequestMethod.PUT)
	public ResponseEntity<CommonResponse> updateUserSkillDetails(
			@ApiParam(name = "userId", value = "User id to get skills") @PathVariable(value="userId") int userId, @ApiParam(name = "skillsList", value = "List of skills") @RequestParam(value="skillsList") List<Integer> skillsList) throws AuthenticationException {

		CommonResponse<String> response = null; 
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(authUserDetails.getUserId() == userId) {
			try {
				
				skillService.updateSkills(userId,skillsList);
				response = ResponseUtility.getResponse("200","Successfully updated skill details to user", "");
			}catch(Exception ex) {
				logger.error("Error while retriving the skill details for userId->"+userId+"<-" , ex);
				response = ResponseUtility.getResponse("10501", "Error while updating skill details for user", "Error while updating the skill details for userId: "+userId);
			}
		}else {
			response = ResponseUtility.getResponse("403", "You don't have the permission to retrive the skills for given user id","Access Denied");
		}
		
		return new ResponseEntity<CommonResponse>(response,HttpStatus.CREATED);			

	}
	
}
