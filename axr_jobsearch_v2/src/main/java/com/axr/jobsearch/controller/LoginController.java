package com.axr.jobsearch.controller;


import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.common.exception.LoginAuthenticationException;
import com.axr.jobsearch.dataaccess.Country;
import com.axr.jobsearch.dataaccess.model.users.AuthToken;
import com.axr.jobsearch.dataaccess.model.users.Login;
import com.axr.jobsearch.security.AuthUserDetails;
import com.axr.jobsearch.service.UserService;
import com.axr.jobsearch.service.users.AuthenticationService;
import com.axr.jobsearch.utils.Validations;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RestController	
public class LoginController {
	
	static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	AuthenticationService authenticationService;
	
	@Autowired
	UserService userService;
	
	@ApiOperation(value = "login", tags = "Authentication", notes = "This API is used to login by user credentials"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>username</td> <td>Mandatory</td> <td> user name </td></tr>"
			+ "<tr><td>password</td> <td>Mandatory</td> <td> user password</td></tr>"
			+ "</table>")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity login(
			@ApiParam(name = "login", value = "login")@RequestBody Login login) throws AuthenticationException {
		logger.error("login attempt 111 " + login.getUsername());
		AuthToken authToken = null;
		try {
			authToken = authenticationService.login(login);
		}catch(Exception ex) {
			logger.error("Auth Error", ex);
		}
		
			//logger.error("authToken -->"+authToken.getAccessToken()+"<--");
			
			if (null == authToken || null == authToken.getAccessToken()) {
				return new ResponseEntity<>("メールアドレスまたはパスワードが一致しません",HttpStatus.UNAUTHORIZED);
			}
			HttpStatus status = authToken.getAccessToken() != null ? HttpStatus.OK : HttpStatus.UNAUTHORIZED;
			return new ResponseEntity<>(authToken, status);			

	}
	
	@ApiOperation(value = "Forgot Password", tags = "Authentication", notes = "This API is used to get the email to reset the password"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>emailAddress</td> <td>Mandatory</td> <td> Email Address </td></tr>"
			+ "</table>")
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
	public ResponseEntity<String> forgotPassword(
			@ApiParam(name = "emailAddress", value = "Email Address")@RequestParam String emailAddress) throws AuthenticationException {

		return new ResponseEntity<>(userService.forgotPassword(emailAddress),HttpStatus.OK);


	}

	@ApiOperation(value = "Reset Password", tags = "Authentication", notes = "This API is used to reset the password"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>validationCode</td> <td>Mandatory</td> <td> Token to reset password </td></tr>"
			+ "</table>")
	@RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
	public ResponseEntity<String> resetPasswordHtml(
			@ApiParam(name = "verificationCode", value = "verificationCode")@RequestParam String verificationCode) throws AuthenticationException {

		return new ResponseEntity<>(userService.resetPasswordToken(verificationCode),HttpStatus.OK);
	}

	@ApiOperation(value = "Reset Password", tags = "Authentication", notes = "This API is used to reset the password"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>validationCode</td> <td>Mandatory</td> <td> Token to reset password </td></tr>"
			+ "<tr><td>Password</td> <td>Mandatory</td> <td> Password </td></tr>"
			+ "<tr><td>Confirm Password</td> <td>Mandatory</td> <td> Confirm Password</td></tr>"			
			+ "</table>")
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public ResponseEntity<String> resetPassword(
			@ApiParam(name = "verificationCode", value = "Verification Code")@RequestParam String verificationCode, 
			@ApiParam(name = "password", value = "Password")@RequestParam String password, 
			@ApiParam(name = "confirmPassword", value = "Confirm Password")@RequestParam String confirmPassword) throws AuthenticationException {

		String result = Validations.validatePassword(password, confirmPassword);
		if(result.equalsIgnoreCase(Constants.SUCCESS)) {
			return new ResponseEntity<>(userService.resetPassword(verificationCode,password),HttpStatus.OK); 
		}else return new ResponseEntity<String>(result,HttpStatus.OK);
	}
	
	@ApiOperation(value = "Logout", tags = "Authentication", notes = "This API is used to logout from the application")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/axrlogout", method = RequestMethod.GET)
	public ResponseEntity<String> axrLogout() {		
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		userService.axrLogout(authUserDetails.getUserId(),authUserDetails.getToken());
		return new ResponseEntity<>("Logged out successfully",HttpStatus.OK);
	}

}
