package com.axr.jobsearch.controller;


import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.axr.jobsearch.common.CommonResponse;
import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.common.exception.LoginAuthenticationException;
import com.axr.jobsearch.common.validator.UserValidator;
import com.axr.jobsearch.dataaccess.Country;
import com.axr.jobsearch.dataaccess.model.users.AuthToken;
import com.axr.jobsearch.dataaccess.model.users.DocumentDetails;
import com.axr.jobsearch.dataaccess.model.users.Languages;
import com.axr.jobsearch.dataaccess.model.users.Login;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.users.valueobjects.UserProfileVO;
import com.axr.jobsearch.security.AuthUserDetails;
import com.axr.jobsearch.service.UserService;
import com.axr.jobsearch.service.users.AuthenticationService;
import com.axr.jobsearch.utils.ConfigMap;
import com.axr.jobsearch.utils.EmailUtility;
import com.axr.jobsearch.utils.ResponseUtility;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RestController	
public class UserController {
	
	static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	//TODO: Update field specific details for documentation
	@ApiOperation(value = "users", tags = "Users", notes = "This API is used to sign-up the new users"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>First Name</td> <td>Mandatory</td> <td> First Name </td></tr>"
			+ "<tr><td>Last Name</td> <td>Mandatory</td> <td> Last Name </td></tr>"
			+ "<tr><td>Kana First Name</td> <td>Mandatory</td> <td> Kana First Name </td></tr>"
			+ "<tr><td>Kana Last Name</td> <td>Mandatory</td> <td> Kana Last Name </td></tr>"
			+ "<tr><td>Date Of Birth</td> <td>Mandatory</td> <td> Date Of Birth in (YYYY-MM-DD) format </td></tr>"
			+ "<tr><td>Mobile Number</td> <td>Mandatory</td> <td> Mobile Number including country code </td></tr>"
			+ "<tr><td>Email Address</td> <td>Mandatory</td> <td> Email Address </td></tr>"
			+ "<tr><td>Password</td> <td>Mandatory</td> <td> Password to meet the criteria like minimum length of 8 chars and mix of numerals, special chars, upper & lower case of alphabets </td></tr>"
			+ "<tr><td>Gender</td> <td>Mandatory</td> <td> Gender info M for Male, F for Female, X for Others </td></tr>"
			+ "<tr><td>Prefecture Id</td> <td>Mandatory</td> <td> Prefecture identifier </td></tr>"
			+ "<tr><td>Role Id</td> <td>Mandatory</td> <td> Role identifier </td></tr>"
			+ "<tr><td>Prefecture Id</td> <td>Mandatory</td> <td> Prefecture identifier </td></tr>"
			+ "<tr><td>User Type Id</td> <td>Mandatory</td> <td> User Type identifier </td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> registerUser(
			@ApiParam(name = "Create/Register new user", value = "") @RequestBody UserProfileVO userProfileVO) throws AuthenticationException {

		logger.error("\n\n**** received emailAddress-->"+userProfileVO.getEmailAddress()+"<==\n\n");
		
		CommonResponse response = null; 
		UserProfile userProfile = null;
		ArrayList<String> errorList = null;
		try {
			userProfile = new UserProfile();
			userProfile.setFirstName(userProfileVO.getFirstName());
			userProfile.setLastName(userProfileVO.getLastName());
			userProfile.setKanaFirstName(userProfileVO.getKanaFirstName());
			userProfile.setKanaLastName(userProfileVO.getKanaLastName());
			userProfile.setEmailAddress(userProfileVO.getEmailAddress());
			userProfile.setPassword(userProfileVO.getPassword());
			userProfile.setMobileNumber(userProfileVO.getMobileNumber());
			userProfile.setGender(userProfileVO.getGender());
			userProfile.setPrefectureId(userProfileVO.getPrefectureId());
			//userProfile.setDateOfBrith(new SimpleDateFormat("yyyy-MM-dd").parse(userProfileVO.getDateOfBrith()));
			userProfile.setDateOfBirth(userProfileVO.getDateOfBirth());
			errorList = UserValidator.validateUser(userProfile.getPassword(), userProfile.getEmailAddress(), userProfile.getDateOfBirth() ,	userProfile.getFirstName() , userProfile.getLastName() , userProfile.getKanaFirstName(), userProfile.getKanaLastName(), userProfile.getMobileNumber());
			if(errorList.size()==0) {
				userProfile = userService.registerUser(userProfile);
				response = ResponseUtility.getResponse("201", userProfile,"Successfully created the user");
			}
			else {
				response = ResponseUtility.getResponse("403", errorList, "Validation error");
			}
		
			
			
			
		}catch(DataIntegrityViolationException  e) {
			logger.error("Error while creating the user ==>"+userProfile.toString()+"<==" , e);
			errorList.add("The email address is already registered");
			response = ResponseUtility.getResponse("403", errorList, "Validation error");
		}catch(Exception ex) {
			logger.error("Error while creating the user -->"+userProfile.toString()+"<--" , ex);
			response = ResponseUtility.getResponse("10201", userProfile, "Error while creating the user");
		}

		return new ResponseEntity<CommonResponse>(response,HttpStatus.CREATED);		

	}
	

	
	//TODO: Update field specific details for documentation
		@ApiOperation(value = "users", tags = "Users", notes = "This API is used to update the users profile"
				+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
				+ "<tr><td>First Name</td> <td>Mandatory</td> <td> First Name </td></tr>"
				+ "<tr><td>Last Name</td> <td>Mandatory</td> <td> Last Name </td></tr>"
				+ "<tr><td>Kana First Name</td> <td>Mandatory</td> <td> Kana First Name </td></tr>"
				+ "<tr><td>Kana Last Name</td> <td>Mandatory</td> <td> Kana Last Name </td></tr>"
				+ "<tr><td>Date Of Birth</td> <td>Mandatory</td> <td> Date Of Birth in (YYYY-MM-DD) format </td></tr>"
				+ "<tr><td>Mobile Number</td> <td>Mandatory</td> <td> Mobile Number including country code </td></tr>"
				+ "<tr><td>Email Address</td> <td>Mandatory</td> <td> Email Address </td></tr>"
				+ "<tr><td>Password</td> <td>Mandatory</td> <td> Password to meet the criteria like minimum length of 8 chars and mix of numerals, special chars, upper & lower case of alphabets </td></tr>"
				+ "<tr><td>Gender</td> <td>Mandatory</td> <td> Gender info M for Male, F for Female, X for Others </td></tr>"
				+ "<tr><td>Prefecture Id</td> <td>Mandatory</td> <td> Prefecture identifier </td></tr>"
				+ "<tr><td>Role Id</td> <td>Mandatory</td> <td> Role identifier </td></tr>"
				+ "<tr><td>Prefecture Id</td> <td>Mandatory</td> <td> Prefecture identifier </td></tr>"
				
				+ "<tr><td>User Type Id</td> <td>Mandatory</td> <td> User Type identifier </td></tr>"
				+ "</table>")
		@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
		@RequestMapping(value = "/users", method = RequestMethod.PUT)
		public ResponseEntity<CommonResponse> updateUser(
				@ApiParam(name = "Update User details", value = "") @RequestBody UserProfileVO userProfileVO) throws AuthenticationException {

			CommonResponse response = null; 
			UserProfile userProfile = null;
			ArrayList<String> errorList = null;
			
			logger.info("\n\n userProfileVO===>"+userProfileVO.getDateOfBirth()+"<==");
			try {
				AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				userProfileVO.setEmailAddress(authUserDetails.getEmailAddress());	
	      		errorList = UserValidator.validateUser(userProfileVO.getPassword(), userProfileVO.getEmailAddress(), userProfileVO.getDateOfBirth() , userProfileVO.getFirstName() , userProfileVO.getLastName() , userProfileVO.getKanaFirstName(), userProfileVO.getKanaLastName(), userProfileVO.getMobileNumber() );
				
				if(errorList.size()==0) {
					userProfile = userService.updateUser(userProfileVO);
                                        DocumentDetails docDetails = userService.getDocumentByUserId(authUserDetails.getUserId());
					if(null != docDetails) userProfile.setDocUrl(docDetails.getDocUrl());

					response = ResponseUtility.getResponse("201", userProfile,"Successfully updated the user");
				}
				else {
					response = ResponseUtility.getResponse("403", errorList,"Validation error");
				}
				
				
			}catch(Exception ex) {
				logger.error("Error while updating the user details-->"+userProfileVO.toString()+"<--" , ex);
				response = ResponseUtility.getResponse("10201", userProfile, "Error while updating the user details");
			}

			return new ResponseEntity<CommonResponse>(response,HttpStatus.CREATED);			

		}
		
		@ApiOperation(value = "users", tags = "Users", notes = "This API is used to retrieve the user profile details")
		@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
		@RequestMapping(value = "/users", method = RequestMethod.GET)
		public ResponseEntity<CommonResponse> getUserDetails() throws AuthenticationException {

			CommonResponse<UserProfile> response = null; 
			UserProfile userProfile = null;
			
			AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			try {
				
				userProfile = userService.retriveUser(authUserDetails.getUserId());
				response = ResponseUtility.getResponse("200",userProfile, "");
			}catch(Exception ex) {
				logger.error("Error while retriving the user details for userId-->"+authUserDetails.getUserId()+"<--" , ex);
				response = ResponseUtility.getResponse("10501", userProfile, "Error while retrieving the user details");
			}

			return new ResponseEntity<CommonResponse>(response,HttpStatus.OK);			

		}
	
         
		
		@ApiOperation(value = "Activate User", tags = "Users", notes = "This API is used to activate the users by verifing the code sent via email")
		@GetMapping("/activate")
		public ResponseEntity<String> activateUser(@ApiParam(name = "verificationCode", value = "") @RequestParam String verificationCode) {
			return new ResponseEntity<>(userService.activateUser(verificationCode),HttpStatus.OK);
		}

		@ApiOperation(value = "Prefecture details", tags = "Common", notes = "This API is used to retrive the list of prefecture for given country")
		@GetMapping("/prefectures")
		public ResponseEntity<List<Prefectures>> getPrefectures(@ApiParam(name = "countryId", value = "1") @RequestParam int countryId) {
			return new ResponseEntity<>(userService.getPrefectures(countryId),HttpStatus.OK);
		}

		@ApiOperation(value = "Language details", tags = "Common", notes = "This API is used to retrive the list of suppported languages")
		@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
		@RequestMapping(value = "/languages",  method = RequestMethod.GET)
		public ResponseEntity<List<Languages>> getLanguages() {			
			return new ResponseEntity<>(userService.getLanguages(),HttpStatus.OK);
		}

		@ApiOperation(value = "Language details", tags = "Common", notes = "This API is used to update the prefered language for the user")
		@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
		@RequestMapping(value = "/languages",  method = RequestMethod.PUT)
		public ResponseEntity<String> updateLanguages(@ApiParam(name = "languageId", value = "1") @RequestParam int languageId) {	
			AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return new ResponseEntity<>(userService.updateLanguage(authUserDetails.getUserId(), languageId),HttpStatus.OK);
		}
		
		@ApiOperation(value = "users", tags = "Common", notes = "This API is used to upload the files")
		@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
		@RequestMapping(value = "/documents", method = RequestMethod.POST)
		public ResponseEntity<CommonResponse> uploadFiles(@RequestParam("document") MultipartFile file,  @RequestParam("docCategoryId") int docCategoryId,  @RequestParam (required = false) Integer orgId, @RequestParam (required = false) Integer jobId) throws AuthenticationException {

			CommonResponse<DocumentDetails> response = null; 
			DocumentDetails documentDetails = null;
			
			AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			try {
				documentDetails = userService.saveDocument(file,docCategoryId,authUserDetails.getUserId(),orgId,jobId);
				
				response = ResponseUtility.getResponse("200",documentDetails,"Document  added successfully");
			}catch(Exception ex) {
				logger.error("Error while saving the documents userId-->"+authUserDetails.getUserId()+"<-- docCategoryId-->"+docCategoryId+"<--" , ex);
				response = ResponseUtility.getResponse("10501", documentDetails, "Error while saving the document details");
			}

			return new ResponseEntity<CommonResponse>(response,HttpStatus.CREATED);			

		}
		
		
		@ApiOperation(value = "users", tags = "Common", notes = "This API is used to download the files")
		@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
		@RequestMapping(value = "/documents/{docId}", method = RequestMethod.GET)
		public ResponseEntity<Resource> downloadFiles(@ApiParam(name = "docId", required=true, value = "docId")@PathVariable(value="docId") int docId) throws AuthenticationException {

			DocumentDetails documentDetails = null;
			Resource resource = null;
			Path storePath = null;
			
			AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			try {
				documentDetails = userService.getDocument(docId,authUserDetails.getUserId());
				
				if(documentDetails.getDocPath() !=null && !documentDetails.getDocPath().isEmpty()) {
					try {
						storePath = Paths.get(ConfigMap.getInstance().getConfigCategoryMap().get(Constants.DOCUMENTS).get(Constants.FILE_STORE_PATH));
						
						Path filePath = storePath.resolve(documentDetails.getDocName());
						resource = new UrlResource(filePath.toUri());
						
						if(resource.exists()) {
							String contentType = documentDetails.getDocType();
							if(null == contentType || "".equalsIgnoreCase(contentType)) {
								contentType = "application/octet-stream";
							}
							
							 return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType)).contentLength(filePath.toFile().length()).header(HttpHeaders.CONTENT_ENCODING,"UTF-8").header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + resource.getFilename() +"").body(resource);
			            } else {
			                throw new FileNotFoundException("File not found " + documentDetails.getDocName());
			            }
						
					}catch (Exception e) {
						logger.error("Error while reading the file content for doc path->"+documentDetails.getDocPath()+"<-");
					}
				}else {
					return ResponseEntity.notFound().build();
				}
					
					        
					
					            
					
					        
			}catch(Exception ex) {
				logger.error("Error while getting the document userId-->"+authUserDetails.getUserId()+"<-- docId-->"+docId+"<--" , ex);
				
			}

			//return new ResponseEntity<CommonResponse>(response,HttpStatus.OK);	
			return ResponseEntity.notFound().build();

		}
		
}
