package com.axr.jobsearch.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.axr.jobsearch.common.CommonResponse;
import com.axr.jobsearch.dataaccess.model.users.SkillDetails;
import com.axr.jobsearch.service.SkillService;
import com.axr.jobsearch.service.UtilityService;
import com.axr.jobsearch.utils.ConfigMap;
import com.axr.jobsearch.utils.ResponseUtility;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@Component
@RestController	
public class AdminController {
	static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	UtilityService utilityService;
	
	@ApiOperation(value = "admin", tags = "Administration", notes = "This API is used to refresh the config params")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/admin/config", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> refreshConfigParams() throws AuthenticationException {

		CommonResponse response = null;
		
		try {
		  	ConfigMap.getInstance().setConfigCategoryMap(utilityService.getConfigParameters());
		  	response = ResponseUtility.getResponse("200", "Refreshed all the config params.", "Refreshed all the config params");
	    }catch(Exception ex) {
		  logger.error("Error while loading",ex);
		  response = ResponseUtility.getResponse("500", "Unable to refresh the config params", "Error while reloading the config paramss");
	    }
		
		return new ResponseEntity<CommonResponse>(response,HttpStatus.OK);			

	}
}
