package com.axr.jobsearch.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.axr.jobsearch.common.CommonResponse;
import com.axr.jobsearch.dataaccess.model.notifications.Notifications;
import com.axr.jobsearch.dataaccess.notifications.NotificationDataMapper;
import com.axr.jobsearch.service.NotificationService;
import com.axr.jobsearch.utils.ResponseUtility;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Component
@RestController
public class NotificationController {
	
	static final Logger logger = LoggerFactory.getLogger(NotificationController.class);

	@Autowired
	NotificationService notificationService;
	
	
	
	@Autowired
	NotificationDataMapper notificationDataMapper;
	

	@ApiOperation(value = "Notifications", tags = "Notifications", notes = "This API is used to get all notifications for particular user")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header")
	@RequestMapping(value = "/{userId}/notifications", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> inviteUser(
	        @ApiParam(name = "userId", value = "userId") @PathVariable(value = "userId") int userId)

			throws AuthenticationException {
		CommonResponse response = null;
		List<Notifications> notification = null;
		try {
			
			notification = notificationService.getUserNotification(userId);
			response = ResponseUtility.getResponse("201", notification, "Successfully retrived");

		}
		catch(Exception ex)
		{
			System.out.println(ex);
			response = ResponseUtility.getResponse("10501", notification, "Error while retriving the user notifications");

		}
        return new ResponseEntity<CommonResponse>(response, HttpStatus.CREATED);
	
	}
	
	@ApiOperation(value = "InviteUser", tags = "Notifications", notes = "This API is used to view the particular Notification")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header")
	@RequestMapping(value = "/notifications/{notificationId}", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> getNotificationById(
	        @ApiParam(name = "notificationId", value = "notificationId") @PathVariable(value = "notificationId") int notificationId)

			throws AuthenticationException {
		CommonResponse response = null;
		Notifications notification = null;
		try {
			
			notification = notificationService.getNotificationById(notificationId);
			response = ResponseUtility.getResponse("201", notification, "Successfully retrived");

		}
		catch(Exception ex)
		{
			System.out.println(ex);
			response = ResponseUtility.getResponse("10501", notification, "Error while retriving the notification details");

		}
        return new ResponseEntity<CommonResponse>(response, HttpStatus.CREATED);
	
	}
	
}
