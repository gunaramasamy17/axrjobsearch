package com.axr.jobsearch.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.axr.jobsearch.common.CommonResponse;
import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.dataaccess.model.jobs.Benefits;
import com.axr.jobsearch.dataaccess.model.jobs.JobFeedback;
import com.axr.jobsearch.dataaccess.model.jobs.Jobs;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobHistoryVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobsVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.RatingVO;
import com.axr.jobsearch.security.AuthUserDetails;
import com.axr.jobsearch.service.JobService;
import com.axr.jobsearch.utils.DynamicFilter;
import com.axr.jobsearch.utils.JobFilterCriteria;
import com.axr.jobsearch.utils.ResponseUtility;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Component
@RestController	
public class JobController {
	static final Logger logger = LoggerFactory.getLogger(JobController.class);
	
	@Autowired
	JobService jobService;
	
	@ApiOperation(value = "Favorites", tags = "Jobs", notes = "This API is used to add jobs to the favorites"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>jobId</td> <td>Mandatory</td> <td> Job identifier </td></tr>"
			+ ""
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/favorites", method = RequestMethod.POST)
	public ResponseEntity<String> addFavorites(
			@ApiParam(name = "jobId", value = "jobId")@RequestParam String jobId) {
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return new ResponseEntity<>(jobService.addFavorites(authUserDetails.getUserId(),Integer.parseInt(jobId)),HttpStatus.OK);
	}
	
	@ApiOperation(value = "Favorites", tags = "Jobs", notes = "This API is used to remove jobs from the favorites"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>jobId</td> <td>Mandatory</td> <td> Job identifier </td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/favorites", method = RequestMethod.DELETE)
	public ResponseEntity<String> removeFavorites(
			@ApiParam(name = "jobId", value = "jobId")@RequestParam String jobId) {
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return new ResponseEntity<>(jobService.removeFavorites(authUserDetails.getUserId(),Integer.parseInt(jobId)),HttpStatus.OK);

	}
	
	@ApiOperation(value = "Job", tags = "Jobs", notes = "This API is used to get a particular job"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>jobId</td> <td>Mandatory</td> <td> Job identifier </td></tr>"
			+ "<tr><td>orgId</td> <td>Mandatory</td> <td> Organization identifier</td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/jobs/{jobId}", method = RequestMethod.GET)
	public ResponseEntity<JobsVO> getJobsById(@ApiParam(name = "jobId", value = "jobId")@PathVariable(value="jobId") int jobId) {
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		return new ResponseEntity<JobsVO>(jobService.getJobsById(jobId,authUserDetails.getUserId()),HttpStatus.OK);

	}
	
	@ApiOperation(value = "Apply Job", tags = "Jobs", notes = "This API is used to apply for a particular job"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>jobId</td> <td>Mandatory</td> <td> Job identifier </td></tr>"
			+ "<tr><td>orgId</td> <td>Mandatory</td> <td> Organization identifier</td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/application/{jobId}", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> jobApplication(@ApiParam(name = "jobId", required=true, value = "jobId")@PathVariable(value="jobId") int jobId,
			@ApiParam(name = "status", required=true, value = "status")@RequestParam String status) {
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		
	
		CommonResponse response = null;
		
		try {
			
			
			
		Instant deadline = jobService.getApplicationDeadline(jobId);		
		int applicationId = jobService.getApplication(jobId, authUserDetails.getUserId());
		
		if (deadline.isAfter(Instant.now())) {
			
			if(applicationId > 0) {
				response = ResponseUtility.getResponse("203", null, "この求人にもう応募しました");
				return new ResponseEntity<>(response,HttpStatus.OK);				
			} else {
				
				jobService.addJobApplication(jobId, authUserDetails.getUserId());
				response = ResponseUtility.getResponse("201", null, "求人に応募しました");
				return new ResponseEntity<>(response,HttpStatus.CREATED);
			}
		}else
		{
			response = ResponseUtility.getResponse("1021", null, "申し込み締切後で申し込んだので応募できませんでした");
			return new ResponseEntity<>(response,HttpStatus.OK);
			
		}
		}catch (Exception ex) {
			
			response = ResponseUtility.getResponse("1051", null, "Error while applying for the job");
			return new ResponseEntity<>(response,HttpStatus.OK);
			
		}	
		


	}
	
	@ApiOperation(value = "Update Job", tags = "Jobs", notes = "This API is used to update the details for a particular job "
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>jobId</td> <td>Mandatory</td> <td> Job identifier </td></tr>"
			+ "<tr><td>orgId</td> <td>Mandatory</td> <td> Organization identifier</td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/application/{jobId}", method = RequestMethod.PUT)
	public ResponseEntity<String> jobApplicationUpdate(@ApiParam(name = "jobId", required=true, value = "jobId")@PathVariable(value="jobId") int jobId,
			@ApiParam(name = "status", required=true, value = "status")@RequestParam String status,
			@ApiParam(name = "penaltyApplied", required=false, value = "0")@RequestParam (required = false, defaultValue="0") int penaltyApplied,
			@ApiParam(name = "remarks", required=false, value = "")@RequestParam (required = false, defaultValue="")  int remarks) {
		
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		return new ResponseEntity<>(jobService.updateJobApplication(jobId,authUserDetails.getUserId(),status,penaltyApplied,remarks),HttpStatus.OK);

	}
	
	@ApiOperation(value = "Job Filter", tags = "Jobs", notes = "This API is used to get the jobs matching the filter criteria"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>list</td> <td>Mandatory</td> <td> openJobs, favoriteJobs, appliedJobs </td></tr>"
			+ "<tr><td>filterByDate</td> <td>Optional</td> <td> Date in YYYY-MM-dd format</td></tr>"
			+ "<tr><td>filterByPrefecture</td> <td>Optional</td> <td> Prefecture id Ex 1,2,3 </td></tr>"
			+ "<tr><td>filterByJobType</td> <td>Optional</td> <td> JobType id Ex 1,2,3 </td></tr>"
			+ "<tr><td>filterByJobStatus</td> <td>Optional</td> <td> JobStatus  Ex Approved, Applied, Rejected, Cancelled </td></tr>"
			+ "<tr><td>filterByBenefits</td> <td>Optional</td> <td> Benefit id Ex 1,2,3 </td></tr>"
			+ "<tr><td>filterByTime</td> <td>Optional</td> <td> start & end time of job Ex 10:00-11:00,16:00-18:00 </td></tr>"
			+ "<tr><td>filterBySalaryRange</td> <td>Optional</td> <td> Salary range Ex 0-1000,1000-5000,5000-10000 </td></tr>"
			+ "<tr><td>sortBy</td> <td>Optional</td> <td> Sort by any one field. Ex 1 - Popularity, 2 - Newest First, 3 - Salary Low to High, 4 - Salary High to Low </td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/jobs", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> filterJobs(
			@ApiParam(name = "list", value = "list")@RequestParam String list,
			@ApiParam(name = "filterByDate", required = false, value = "filterByDate")@RequestParam (required = false) List<String> filterByDate,
			@ApiParam(name = "filterByPrefecture", required = false, value = "filterByPrefecture")@RequestParam (required = false) List<Integer> filterByPrefecture,
			@ApiParam(name = "filterByJobType", required = false, value = "filterByJobType")@RequestParam (required = false) List<Integer> filterByJobType,
			@ApiParam(name = "filterByJobStatus", required = false, value = "filterByJobStatus")@RequestParam (required = false) List<String> filterByJobStatus,
			@ApiParam(name = "filterByBenefits", required = false, value = "filterByBenefits")@RequestParam (required = false) List<Integer> filterByBenefits,
			@ApiParam(name = "filterByTime", required = false, value = "filterByTime") @RequestParam (required = false) List<String> filterByTime,
			@ApiParam(name = "filterBySalaryRange", required = false, value = "filterBySalaryRange")@RequestParam (required = false) List<String> filterBySalaryRange,
			@ApiParam(name = "sortBy", required = false, value = "sortBy")@RequestParam (required = false, defaultValue ="1") int sortBy) {
		
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		CommonResponse response = null;
		
		if(list.equalsIgnoreCase(Constants.OPEN_JOBS) || list.equalsIgnoreCase(Constants.FAVORITE_JOBS) || list.equalsIgnoreCase(Constants.APPLIED_JOBS)) {
		
			JobFilterCriteria jobFilterCriteria = new JobFilterCriteria();
			
			if(null != filterByDate && filterByDate.size()>0) jobFilterCriteria.setDate(filterByDate.get(0));
			jobFilterCriteria.setBenefitsList(filterByBenefits);
			jobFilterCriteria.setJobTypeList(filterByJobType);
			jobFilterCriteria.setJobStatusList(filterByJobStatus);
			jobFilterCriteria.setList(list);
			jobFilterCriteria.setPrefectureList(filterByPrefecture);
			jobFilterCriteria.setSortBy(sortBy);
			jobFilterCriteria.setUserId(authUserDetails.getUserId());
			jobFilterCriteria.setFilterByTime(filterByTime);
			jobFilterCriteria.setFilterBySalaryRange(filterBySalaryRange);
			
			List<Jobs> jobsList = jobService.getJobsByFilters(jobFilterCriteria);
			
			if(jobFilterCriteria.getSortBy() == 1) {
				HashMap<Integer,List<Jobs>> jobsMap = new HashMap<Integer,List<Jobs>>();
				List<Jobs> jList = null;
				//TODO: Potential performance improvement
				for(Jobs j : jobsList) {					
					if(jobsMap.containsKey(j.getOrgId())) jList = jobsMap.get(j.getOrgId());
					else jList = new ArrayList<Jobs>();				
					jList.add(j);
					jobsMap.put(j.getOrgId(), jList);
				}

				List<Jobs> jobsListSorted = new ArrayList<Jobs>();
				List<RatingVO> ratingList = jobService.getJobFeedbackTotalRating();
				
				for(RatingVO rVO : ratingList) {
					if(jobsMap.containsKey(rVO.getOrgId())) {
						jobsListSorted.addAll(jobsMap.get(rVO.getOrgId()));			
						jobsMap.remove(rVO.getOrgId());
					}
				}
				
				if(jobsMap.size()>0) {
					Iterator <Integer> itr = jobsMap.keySet().iterator();
					int oId;
					while(itr.hasNext())  {
						oId = (int)itr.next();
						jobsListSorted.addAll(jobsMap.get(oId));
					}
				}
				jobsList = jobsListSorted;
			}
	
			HashMap<Integer,Jobs> favoriteJobs = jobService.getFavoriteJobs(authUserDetails.getUserId());
			
			int index=0;
			List<Integer> jobIdList = new ArrayList<Integer>();
			for(Jobs job : jobsList) {
				if(favoriteJobs.containsKey(job.getJobId())) {
					job.setFavorite(true);
					jobsList.set(index, job);
				}
				jobIdList.add(job.getJobId());
				index++;
			}
			
			HashMap<Integer,List<String>> jobImagesMap = jobService.getJobImagesList(jobIdList);
			index=0;
			for(Jobs job : jobsList) {
				if(jobImagesMap.containsKey(job.getJobId())) {
					job.setJobImagesList(jobImagesMap.get(job.getJobId()));
					jobsList.set(index, job);
				}
				index++;
			}
			
			
			response = ResponseUtility.getResponse("200", jobsList ,"Jobs");
			return new ResponseEntity<>(response,HttpStatus.OK);

		}else {
			response = ResponseUtility.getResponse("403", "Invalid value for list parameter" ,"Validation errors");
			return new ResponseEntity<CommonResponse>(response,HttpStatus.OK);	
		}
	}
	
	@ApiOperation(value = "Job Logtime", tags = "Jobs", notes = "This API is used to add the log details for a particular job"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>jobId</td> <td>Mandatory</td> <td> Job identifier </td></tr>"
			+ "<tr><td>orgId</td> <td>Mandatory</td> <td> Organization identifier</td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/jobs/{orgId}/logtime", method = RequestMethod.POST)
	public ResponseEntity<CommonResponse> jobLogTime(
			@ApiParam(name = "orgId", value = "orgId")@PathVariable(value="orgId") int orgId,
			@ApiParam(name = "logType", value = "logType")@RequestParam String logType,
			@ApiParam(name = "logTime", value = "logTime")@RequestParam String logTime) {
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				CommonResponse response = null;
		
		try {
		Integer jobId = jobService.getJobId(authUserDetails.getUserId(),orgId,new java.util.Date());
		
		
		 if ( null != jobId && jobId > 0) {
			
			int count =  jobService.jobLogRecordCount(authUserDetails.getUserId(),  jobId,  logType);
			if(count == 0) {
				
				jobService.jobLogTime(authUserDetails.getUserId(), jobId, logType, logTime, Instant.now()); 
				response = ResponseUtility.getResponse("201", "","Successfully updated the Logtime");
				return new ResponseEntity<>(response,HttpStatus.CREATED);
			}else {
				
				response = ResponseUtility.getResponse("203", "", "Already data exists for this action");
				return new ResponseEntity<>(response,HttpStatus.OK);
				
			}
			
		}else {
				
				response = ResponseUtility.getResponse("1021", "", "You don't have any approved jobs for this organization at this point of time");
				return new ResponseEntity<>(response,HttpStatus.OK);
		}			
			
			
		}	catch(Exception ex) {
			response = ResponseUtility.getResponse("1051", null, "Error while updating the details");
			logger.error("Error while updating the details for user-->"+authUserDetails.getUserId()+"<--" , ex);
			return new ResponseEntity<CommonResponse>(response,HttpStatus.OK);	
		}

		
		
	}
	
	@ApiOperation(value = "Job Feedback", tags = "Jobs", notes = "This API is used to retrieve the job feedback"
			+ "<br><table>" + "<tr><td>Attribute Name</td>   <td>Mandatory/Optional</td>   <td>Description</td></tr>"
			+ "<tr><td>jobId</td> <td>Mandatory</td> <td> Job identifier </td></tr>"
			+ "<tr><td>orgId</td> <td>Mandatory</td> <td> Organization identifier</td></tr>"
			+ "</table>")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/jobs/feedback", method = RequestMethod.GET)
	public ResponseEntity<List<JobFeedback>> jobFeedback() {
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return new ResponseEntity<>(jobService.getJobFeedback(authUserDetails.getUserId()),HttpStatus.OK);

	}
	

	@ApiOperation(value = "Benefits details", tags = "Common", notes = "This API is used to retrive the list of Benefits")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header")
	@RequestMapping(value = "/benefits", method = RequestMethod.GET)
	public ResponseEntity<List<Benefits>> getBenefits() {
		return new ResponseEntity<>(jobService.getBenefits(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Past working details", tags = "Jobs", notes = "This API is used to retrive the past working details")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header")
	@RequestMapping(value = "/jobhistory", method = RequestMethod.GET)
	public ResponseEntity<List<JobHistoryVO>> getJobHistory() {
		AuthUserDetails authUserDetails = (AuthUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return new ResponseEntity<>(jobService.getJobHistory(authUserDetails.getUserId()), HttpStatus.OK);
	}
	
	
			
}
