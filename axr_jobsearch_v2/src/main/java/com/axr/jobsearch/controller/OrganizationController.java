package com.axr.jobsearch.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.axr.jobsearch.common.CommonResponse;
import com.axr.jobsearch.dataaccess.model.organization.Organization;
import com.axr.jobsearch.qrcode.QRcodeGenerator;
import com.axr.jobsearch.service.OrganizationService;
import com.axr.jobsearch.utils.ResponseUtility;
import com.google.zxing.WriterException;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Component
@RestController
public class OrganizationController {
	
	static final Logger logger = LoggerFactory.getLogger(OrganizationController.class);
	
	@Autowired
	OrganizationService organizationService;
 
	
	@ApiOperation(value = "Organization", tags = "Common", notes = "This API is used to get the QRcode for a particular organization ")
	@ApiImplicitParam(name = "Authorization", value = "Bearer token", paramType = "header" )
	@RequestMapping(value = "/org/{orgId}/qr", method = RequestMethod.GET)
	public ResponseEntity<CommonResponse> getOrganizationInfo(
			@ApiParam(name = "orgId", value = "Organization id to get info") @PathVariable(value="orgId") int orgId) throws WriterException, IOException{
		
		CommonResponse<String>  response = null;
		Organization organizationDetails = null ;
		
		organizationDetails = organizationService.getOrganizationInfoByOrgId(orgId);
		QRcodeGenerator generator = new QRcodeGenerator();
		String encodedData = generator.writeQRCode(organizationDetails);
		response = ResponseUtility.getResponse("200",encodedData , "");
		return new ResponseEntity<CommonResponse>(response,HttpStatus.OK);		
		
	}
}
