package com.axr.jobsearch.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.common.exception.LoginAuthenticationException;
import com.axr.jobsearch.controller.JobController;

public class AuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {
	
	static final Logger logger = LoggerFactory.getLogger(AuthenticationTokenFilter.class);
	
	public AuthenticationTokenFilter() {
		super("/**");
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		
		String authHeader = request.getHeader(Constants.AUTHORIZATION);
		if(authHeader == null || !authHeader.startsWith(Constants.BEARER)) {
			logger.error("Security Error: Access token missing in the request -->"+request.getRequestURI()+"<-- method->"+request.getMethod()+"<--");
			throw new LoginAuthenticationException("Access token missing in the request",new Exception("Security Error"));
		}
		JwtAuthToken jwtAuthToken = new JwtAuthToken(authHeader.substring(Constants.BEARER.length()));
		return getAuthenticationManager().authenticate(jwtAuthToken);
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		super.successfulAuthentication(request, response, chain, authResult);

		// As this authentication is in HTTP header, after success we need to continue
		// the request normally
		// and return the response as if the resource was not secured at all
		chain.doFilter(request, response);
	}

}
