package com.axr.jobsearch.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.axr.jobsearch.common.validator.JwtTokenValidator;
import com.axr.jobsearch.common.validator.PrivilegeProvider;
import com.axr.jobsearch.dataaccess.model.users.Privileges;

import java.util.ArrayList;
import java.util.List;

@Component
public class AuthProvider extends AbstractUserDetailsAuthenticationProvider {
	
	static final Logger logger = LoggerFactory.getLogger(AuthProvider .class);
	
	@Autowired
	private JwtTokenValidator jwtTokenValidator;
	
	@Autowired
	private PrivilegeProvider privilegeProvider;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException { 
	
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {

		//AuthModel authModel = (AuthModel) authentication;
		//HttpServletRequest request = authModel.getRequest();
		
		//TODO: Retrive the token/cert/credentials from request object & validate with DB 
		//		before populating the authUserDetails object
		
		JwtAuthToken jwtauthToken = (JwtAuthToken) authentication;
		AuthUserDetails authUserDetails = jwtTokenValidator.parseToken(jwtauthToken.getToken());
		
		
		List<String> privilegeList = privilegeProvider.getPrivileges(authUserDetails.getRoleId());
		
		
		//Retrive the privileges from DB for this role & update the authorities in this authUserDetails object
		 
		
		List<GrantedAuthority> authorityList = !privilegeList.isEmpty()
				? AuthorityUtils.commaSeparatedStringToAuthorityList(String.join(",", privilegeList))
				: null;
				
		logger.info("\n\nauthUserDetails email-->"+authUserDetails.getEmailAddress()+"<-- privileges -->"+privilegeList.toString()+"\n\n");
		authUserDetails = authUserDetails.toBuilder().authorities(authorityList).build();
	
		return authUserDetails;
	}
	
	@Override
	public boolean supports(Class<?> authentication) {
		return JwtAuthToken.class.isAssignableFrom(authentication);
	}

}
