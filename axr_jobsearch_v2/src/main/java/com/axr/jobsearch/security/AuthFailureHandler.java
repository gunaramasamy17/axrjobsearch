package com.axr.jobsearch.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.axr.jobsearch.common.exception.ExceptionUtils;

public class AuthFailureHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest arg0, HttpServletResponse response,
			AuthenticationException authenticationException) throws IOException, ServletException {
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		System.out.println("\n\n\n===========>"+authenticationException.getMessage()+"<=============\n\n\n");
		//TODO: Implement custom exception to avoid sending stack trace in response
		response.getOutputStream().println(ExceptionUtils.getErrorResponseBody(null, authenticationException.getMessage()));
	}

}
