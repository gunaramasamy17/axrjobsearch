package com.axr.jobsearch.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.SkillDetails;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.users.valueobjects.UserProfileVO;

@Service
public interface SkillService {

	List<SkillDetails> retriveAllSkills();
	
	List<SkillDetails> retriveSkills(int userId);
	
	void updateSkills(int userId, List<Integer> skillsList);
}

