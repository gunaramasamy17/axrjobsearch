package com.axr.jobsearch.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.dataaccess.jobs.JobDataMapper;
import com.axr.jobsearch.dataaccess.model.jobs.Favorites;
import com.axr.jobsearch.dataaccess.model.users.SkillDetails;
import com.axr.jobsearch.dataaccess.skills.SkillDataMapper;
import com.axr.jobsearch.service.JobService;
import com.axr.jobsearch.service.SkillService;



@Service
public class SkillServiceImpl implements SkillService {
	
	static final Logger logger = LoggerFactory.getLogger(SkillServiceImpl.class);
	
	@Autowired
	private SkillDataMapper skillDataMapper;

	@Override
	public List<SkillDetails> retriveAllSkills() {
		
		return skillDataMapper.retriveAllSkills();
	}

	@Override
	public List<SkillDetails> retriveSkills(int userId) {
		return skillDataMapper.retriveSkillsByUserId(userId);
	}

	@Override
	@Transactional
	public void updateSkills(int userId, List<Integer> skillIdList) {
		
		skillDataMapper.deleteUserSkills(userId);
		for(int skillId : skillIdList) {
			skillDataMapper.addUserSkills(userId, skillId);
		}
	}

	
	
	

}
