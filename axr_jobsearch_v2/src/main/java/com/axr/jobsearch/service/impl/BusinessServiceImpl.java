package com.axr.jobsearch.service.impl;

import java.util.List;

import com.axr.jobsearch.dataaccess.DataMapper;
import com.axr.jobsearch.dataaccess.Country;
import com.axr.jobsearch.service.BusinessService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusinessServiceImpl implements BusinessService {
	
	@Autowired
	private DataMapper dataMapper;
	
	static final Logger logger = LoggerFactory.getLogger(BusinessServiceImpl.class);
	
	@Override
	public List<Country> getCountryList() {

		
		List<Country> countryList = dataMapper.getCountryList();
		logger.error("countryList size-->"+countryList.size()+" Content-->"+countryList.toString()+"<--");
		return countryList;
		//return dataMapper.getCountryList();
	}

}
