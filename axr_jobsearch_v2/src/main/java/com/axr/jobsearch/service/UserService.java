package com.axr.jobsearch.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.axr.jobsearch.dataaccess.model.users.DocumentDetails;
import com.axr.jobsearch.dataaccess.model.users.Languages;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.users.valueobjects.UserProfileVO;

@Service
public interface UserService {

	UserProfile addUser(UserProfileVO userProfileVO);
	
	void delete(long userId);
	
	UserProfile updateUser(UserProfileVO userProfileVO);
	
	UserProfile registerUser(UserProfile userProfile);
	
	UserProfile retriveUser(int userId);

	
	String activateUser(String verificationCode);
	
	List<Prefectures> getPrefectures(int countryId);
	
	List<Languages> getLanguages();
	
	String updateLanguage(int userId, int languageId);
	
	String forgotPassword(String emailAddress);
	
	String resetPasswordToken(String validationCode);
	
	String resetPassword(String validationCode, String password);
	
	String axrLogout(int userId, String token);
	
	DocumentDetails saveDocument(MultipartFile file, int docCategoryId, int userId,Integer orgId, Integer jobId);
	DocumentDetails getDocument(int docId,int userId);
	
	DocumentDetails getDocumentByUserId(int userId);
	/*
	void resetCredentials(Credentials userCredentials, long userId);
	
	String generateToken(long userId);

	*/
	
}

