package com.axr.jobsearch.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.axr.jobsearch.dataaccess.Country;

@Service
public interface BusinessService {

	List<Country> getCountryList();
}

