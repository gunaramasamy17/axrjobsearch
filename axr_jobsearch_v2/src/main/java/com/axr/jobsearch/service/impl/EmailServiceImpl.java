package com.axr.jobsearch.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.email.EmailDataMapper;
import com.axr.jobsearch.dataaccess.model.email.EmailAttribute;
import com.axr.jobsearch.dataaccess.model.email.EmailMessage;
import com.axr.jobsearch.dataaccess.model.email.EmailTemplate;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.service.EmailService;
import com.axr.jobsearch.utils.EmailServer;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class EmailServiceImpl implements EmailService {
	
	static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Autowired
	private EmailDataMapper emailDataMapper;
	

	
	@Override
	public boolean sendEmail(EmailMessage emailMessage) {
		
		Session session = EmailServer.getInstance().getSession();
		Transport transport = EmailServer.getInstance().getTransport();
		
		ArrayList<String> toList = null;
		ArrayList<String> ccList = null;
		ArrayList<String> bccList = null;

		if (emailMessage.getEmailTo() != null && emailMessage.getEmailTo().trim().length() > 0) {
			toList = new ArrayList<String>(Arrays.asList(emailMessage.getEmailTo().split(";")));
		}
		if (emailMessage.getEmailCC() != null && emailMessage.getEmailCC().trim().length() > 0) {
			ccList = new ArrayList<String>(Arrays.asList(emailMessage.getEmailCC().split(";")));
		}
		if (emailMessage.getEmailBcc() != null && emailMessage.getEmailBcc().trim().length() > 0) {
			bccList = new ArrayList<String>(Arrays.asList(emailMessage.getEmailBcc().split(";")));
		}
		
		emailDataMapper.saveEmailMessage(emailMessage);
		
		ArrayList<EmailAttribute> emailAttributeList = emailMessage.getEmailAttributeList();
		for(EmailAttribute emailAttribute : emailAttributeList) {
			emailAttribute.setMessageId(emailMessage.getMessageId());
			emailDataMapper.saveEmailAttribute(emailAttribute);
		}
		
		
		String fromEmail = emailMessage.getEmailFrom();
		String charset = emailMessage.getCharset();

		MimeMessage message = new MimeMessage(session);
		
		try {
				message.setFrom(new InternetAddress(fromEmail));
				message.setHeader("Content-Transfer-Encoding:", "7bit");
				message.setSentDate(java.util.Calendar.getInstance().getTime());
	
				message.setSubject(emailMessage.getSubject(), charset);
				
				HashMap<String,String> attrMap = convertEmailAttributeListToMap(emailAttributeList);
				String body = convertMessageBody(attrMap, emailMessage.getEmailTemplate());
			
				message.setFrom(new InternetAddress(fromEmail));
				if (toList != null) {
					setAddresses(message, toList, javax.mail.Message.RecipientType.TO, charset);
				}
				if (ccList != null) {
					setAddresses(message, ccList, javax.mail.Message.RecipientType.CC, charset);
				}
				if (bccList != null) {
					setAddresses(message, bccList, javax.mail.Message.RecipientType.BCC, charset);
				}

				if (emailMessage.getEmailReplyTo() != null && emailMessage.getEmailReplyTo().trim().length() > 0) {
					message.setReplyTo(new javax.mail.Address[] {
							new javax.mail.internet.InternetAddress(emailMessage.getEmailReplyTo()) });
				}
				
				
				message.setText(body, charset);
				message.setContentID("AbcX132sdfsd4343567672");
				setContentTypeHeader(message, emailMessage.getContentType(), charset);


			
			
		}catch(Exception ex) {
			logger.error("Error while sending an email",ex);
		}
		
		try {
			boolean mailSentStatus = send(message, emailMessage, transport);
		} catch (MessagingException me) {
			logger.error("Failed to send an email",me);
			//throw new Exception("Failed to send the mail", me);
		}
		
		return false;
	}
	
	
	
	protected boolean send(MimeMessage message, EmailMessage emailMessage, Transport transport)
			throws MessagingException {
		int attempts = 0;
		int sleepRetryTime = 1000;
		boolean messageSent = false;
		MessagingException exception = null;
		int retryAttempts = 1;
		logger.debug("Entered into send for messageId-->"+emailMessage.getMessageId()+"<--");
		while (attempts < retryAttempts && !messageSent) {
			logger.debug("In send while loop messageId-->"+emailMessage.getMessageId()+"<--");
			try {
				sendInternal(message, transport);
				messageSent = true;
			} catch (MessagingException me) {
				if (transport.isConnected()) {
					transport.close();
				}
				try {
					Thread.sleep(sleepRetryTime);
				} catch (InterruptedException ie) {
					logger.error(
							"Error while waiting to retry for messageId->" + emailMessage.getMessageId() + "<-",
							ie);
				}
				exception = me;
				attempts++;
				logger.error("Error while sending an email for messageId->" + emailMessage.getMessageId() + "<-",
						me);
			}
		}
		if (!messageSent) {
			logger.info("unable to send the message :" + message);
			try {
				emailDataMapper.updateEmailStatus(emailMessage.getMessageId(),"FAILED");
			} catch (Exception e) {
				logger.error("Error while updating the failure status for messageId->" + emailMessage.getMessageId()
						+ "<-", e);
			}

		} else {
			logger.info("sent the message:" + message);
			try {
				emailDataMapper.updateEmailStatus(emailMessage.getMessageId(),"SUCCESS");
			} catch (Exception e) {
				logger.error("Error while updating the success status for messageId->" + emailMessage.getMessageId()
						+ "<-", e);
			}
		}
		return messageSent;
	}
	
	protected void sendInternal(MimeMessage message, Transport transport) throws MessagingException {
		logger.debug("Entered into sendInternal");
		synchronized (transport) {
			if (!transport.isConnected()) {
				transport.connect();
			}
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		}
		logger.debug("Exit from sendInternal");
	}
	
	public static HashMap<String,String> convertEmailAttributeListToMap(List<EmailAttribute> emailAttributeList) {
		 HashMap<String,String> attrMap =  new HashMap<String,String>();
		 for(EmailAttribute emailAttribute : emailAttributeList) {
			 attrMap.put(emailAttribute.getAttributeName(), emailAttribute.getAttributeValue());
		 }
		 return attrMap;
	}

	private String convertMessageBody(HashMap<String, String> mapMailAttributeVO, EmailTemplate emailTemplate) {
		// TODO Make use of template pass in email message object
		// EmailTemplate emailTemplate
		// =emailTemplateService.findByCategory(emailCategory);

		String mailTemplate = emailTemplate.getTemplateContent();
		Map<String, Object> model = new HashMap<String, Object>();
		Iterator it = mapMailAttributeVO.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry emailAttribute = (Map.Entry) it.next();
			model.put((String) emailAttribute.getKey(), emailAttribute.getValue());
		}
		String mailBody = mailTemplate;
		logger.info("==>" + mailBody + "<==");
		Template tpl=null;
			try {
				tpl = loadTemplate(mailTemplate);
			} catch (IOException e) {
				logger.error("error in loading email template ->",e);
				throw new EmptyResultDataAccessException("error in loading email template", 1);
			}
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			try {
				processTemplate(tpl, model, bout);
			} catch (TemplateException e) {
				logger.error("error in processing email Template ->",e);
				throw new EmptyResultDataAccessException("error in processing email template", 1);
			} catch (IOException e) {
				logger.error("error in writing email Template for ->",e);
				throw new EmptyResultDataAccessException("error in writing email template", 1);
			}
			return bout.toString();
	}
	
	private Template loadTemplate(String templateName) throws IOException {
		Configuration configuration = new Configuration();
		String templateContent = getTemplateContent(templateName);
		StringTemplateLoader stl = new StringTemplateLoader();
		stl.putTemplate(templateName, templateContent);
		configuration.setTemplateLoader(stl);
		return configuration.getTemplate(templateName);
	}

	private void processTemplate(Template tpl, Map<String, Object> dataModel, ByteArrayOutputStream bout)
			throws TemplateException, IOException {
		tpl.process(dataModel, new OutputStreamWriter(bout));
	}

	private String getTemplateContent(String templateName) throws IOException {
		return templateName;
	}

	@Override
	public EmailTemplate getEmailTemplate(String templateName) {
		return emailDataMapper.getEmailTemplate(templateName);
	}

	@Override
	public int saveEmailAttribute(EmailAttribute emailAttribute) {		
		return emailDataMapper.saveEmailAttribute(emailAttribute);
	}

	@Override
	public int saveEmailMessage(EmailMessage emailMessage) {
		return emailDataMapper.saveEmailMessage(emailMessage);
	}	
	
	protected void setAddresses(MimeMessage message, List<String> addresses,
			javax.mail.Message.RecipientType recipientType, String charset)
			throws MessagingException, UnsupportedEncodingException {
		InternetAddress address = null;
		for (String addr : addresses) {
			address = new InternetAddress(addr);
			message.addRecipient(recipientType, address);
		}
	}
	
	protected void setContentTypeHeader(Part part, String bodyType, String charset) throws MessagingException {
		if (charset != null) {
			part.addHeader("Content-Type", bodyType + "; charset=" + charset);
		} else {
			part.addHeader("Content-Type", bodyType);
		}
	}
	
}
