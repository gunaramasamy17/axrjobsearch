package com.axr.jobsearch.service;

import java.util.HashMap;
import java.util.List;
import java.time.Instant;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.jobs.Benefits;
import com.axr.jobsearch.dataaccess.model.jobs.JobFeedback;
import com.axr.jobsearch.dataaccess.model.jobs.Jobs;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobHistoryVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobsVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.RatingVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.UserProfileVO;

import com.axr.jobsearch.utils.JobFilterCriteria;

@Service
public interface JobService {

	String addFavorites(int userId, int jobId);
	
	String removeFavorites(int userId, int jobId);
	
	HashMap<Integer,Jobs> getFavoriteJobs(int userId);
	
	List<Jobs> getJobsByFilters(JobFilterCriteria jobFilterCriteria);
	
	JobsVO getJobsById(int jobId, int userId);
	
	void addJobApplication(int jobId, int userId);
	
	String updateJobApplication(int jobId, int userId, String status, int penalty, int remarks);
	
	List<JobFeedback> getJobFeedback(int userId);
	
	List<Benefits> getBenefits();
	
	List<Jobs> getAppliedJobsByStatus(String status, int userId);
	
	List<Jobs> getJobsByJobStatus(List<Integer>jobIdList, List<String> filterByJobStatus);
	
	List<JobHistoryVO> getJobHistory(int userId);
	
	Float getJobFeedbackRatingByJobId(int jobId);
	
	List<RatingVO> getJobFeedbackTotalRating();
	
	HashMap<Integer,List<String>> getJobImagesList(List<Integer> jobIdList);
      
       int jobLogRecordCount(int userId, int jobId, String logType);

	Integer getJobId(int userId, int orgId, Date curDate);

	void jobLogTime(int userId, int jobId, String logType, String logTime, Instant currentTime);

	Instant getApplicationDeadline(int jobId);

	int getApplication(int jobId, int userId); 
}
