package com.axr.jobsearch.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.notifications.Notifications;
@Service

public interface NotificationService {


	List<Notifications> getUserNotification(int userId);

	Notifications getNotificationById(int notificationId);


}
