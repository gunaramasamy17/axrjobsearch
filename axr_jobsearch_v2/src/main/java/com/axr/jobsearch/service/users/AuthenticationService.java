package com.axr.jobsearch.service.users;

import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.users.AuthToken;
import com.axr.jobsearch.dataaccess.model.users.Login;

@Service
public interface AuthenticationService {
	
	/**
	 * authenticate the user
	 * @param login - user loginame and password
	 * @return AuthToken - user tokens	 
	 */
	AuthToken login(Login login); 

}
