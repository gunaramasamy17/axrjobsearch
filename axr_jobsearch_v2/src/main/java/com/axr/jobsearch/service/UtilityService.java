package com.axr.jobsearch.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface UtilityService {

	public HashMap<String, HashMap<String,String>> getConfigParameters();
}
