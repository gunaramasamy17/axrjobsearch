package com.axr.jobsearch.service;

import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.organization.Organization;

@Service
public interface OrganizationService {
	
	Organization getOrganizationInfoByOrgId(int orgId);

	

}
