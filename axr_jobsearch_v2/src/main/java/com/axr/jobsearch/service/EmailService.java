package com.axr.jobsearch.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.email.EmailAttribute;
import com.axr.jobsearch.dataaccess.model.email.EmailMessage;
import com.axr.jobsearch.dataaccess.model.email.EmailTemplate;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;

@Service
public interface EmailService {

	boolean sendEmail(EmailMessage emailMessage);
	
	EmailTemplate getEmailTemplate(String templateName);
	
	int saveEmailAttribute(EmailAttribute emailAttribute);
	
	int saveEmailMessage(EmailMessage emailMessage);
	
	//void sendUserInviteEmail(UserProfile userProfile);
}
