package com.axr.jobsearch.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.where.render.WhereClauseProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.dataaccess.jobs.JobDataMapper;
import com.axr.jobsearch.dataaccess.model.jobs.Benefits;
import com.axr.jobsearch.dataaccess.model.jobs.Favorites;
import com.axr.jobsearch.dataaccess.model.jobs.JobFeedback;
import com.axr.jobsearch.dataaccess.model.jobs.Jobs;
import com.axr.jobsearch.dataaccess.model.users.DocumentDetails;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobHistoryVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobsVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.RatingVO;
import com.axr.jobsearch.service.JobService;
import com.axr.jobsearch.utils.JobFilterCriteria;





@Service
public class JobServiceImpl implements JobService {
	
	static final Logger logger = LoggerFactory.getLogger(JobServiceImpl.class);
	
	@Autowired
	private JobDataMapper jobDataMapper;

	@Override
	public String addFavorites(int userId, int jobId) {
		
		String message = "Successfully added into favorites";

		Favorites favorite = new Favorites();
		favorite.setJobId(jobId);
		favorite.setUserId(userId);
		favorite.setIsActive(Constants.STATUS_YES);
		favorite.setCreatedDate(Instant.now());
		favorite.setLastUpdatedDate(Instant.now());
		jobDataMapper.addFavorites(favorite);

		return message;
	}

	@Override
	public String removeFavorites(int userId, int jobId) {
		
		String message = "Successfully removed from favorites";
		
		jobDataMapper.removeFavorites(userId,jobId, Instant.now());
		
		return message;
	}

	@Override
	public List<Jobs> getJobsByFilters(JobFilterCriteria jobFilterCriteria) {

		return jobDataMapper.getJobs(jobFilterCriteria);
	}
	
	@Override
	public void jobLogTime(int userId,int jobId, String logType, String logTime, Instant currentTime) {
		//TODO: Error handling
		jobDataMapper.addJobLogTime(userId,jobId,logType,Instant.parse(logTime),Instant.now());
	}

	@Override
	public JobsVO getJobsById(int jobId, int userId) { 
		JobsVO jobsVO = jobDataMapper.getJobsById(jobId,userId); 
		Float rating = getJobFeedbackRatingByJobId(jobId);
		if(null != jobsVO) {
			if(null != rating) jobsVO.setRating(rating);
			jobsVO.setJobPrerequisiteDocs(jobDataMapper.getJobPrerequisiteDocCategoryNameByJobId(jobId));
			//jobsVO.setBenefitsList(jobDataMapper.getBenefitsByJobId(jobId));
       			DocumentDetails documentDetails = jobDataMapper.getJobImagesListByJobId(jobId);
			if(null != documentDetails) jobsVO.setJobImageUri(documentDetails.getDocUrl());
		}		
		return jobsVO;
	}

	@Override
	public void addJobApplication(int jobId, int userId) {
		//TODO: Error handling
		jobDataMapper.addJobApplication(jobId,userId,Constants.JOB_APPLIED,0,0,Instant.now());
	}

	@Override
	public String updateJobApplication(int jobId, int userId, String status, int penalty, int  remarks) {
		//TODO: Error handling
		String result = "Unknown Error";
		

		jobDataMapper.updateJobApplication(jobId,userId,status,penalty,remarks,Instant.now());
		result = "Successfuly updated the record";
		return result;
	}

	@Override
	public List<JobFeedback> getJobFeedback(int userId) {
		
		return jobDataMapper.getJobFeedback(userId);
	}

	@Override
	public Float getJobFeedbackRatingByJobId(int jobId) {		
		return jobDataMapper.getJobFeedbackRatingByJobId(jobId);
	}

	@Override
	public List<Benefits> getBenefits() {
		// TODO Auto-generated method stub
		return jobDataMapper.getBenefits();
	}

	  
	@Override
	public List<Jobs> getAppliedJobsByStatus(String status, int userId){
		
		return jobDataMapper.getAppliedJobsByStatus(status,userId);
	}

	@Override
	public List<Jobs> getJobsByJobStatus(List<Integer> jobIdList, List<String> filterByJobStatus) {

		logger.info("jobIdList size->"+jobIdList.size()+"<- filterByJobStatus->"+filterByJobStatus.size()+"<-");
		return jobDataMapper.getJobsByJobStatus(jobIdList,filterByJobStatus);

	}

	@Override
	public List<JobHistoryVO> getJobHistory(int userId) {	
		
		List<JobHistoryVO> jhList = jobDataMapper.getJobHistory(userId);
		
		List<JobHistoryVO> jobHistoryList = new ArrayList<JobHistoryVO>();
		List<Integer> jobIdList = new ArrayList<Integer>();
		for(JobHistoryVO jh : jhList) {
			if(!jh.isPenaltyApplied()) {
				jh.setPenaltyCost(0);
			}
			jobHistoryList.add(jh);
			jobIdList.add(jh.getJobId());
		}

		List<DocumentDetails> docDetailsList = null;

		if(null != jobIdList && jobIdList.size()>0) docDetailsList = jobDataMapper.getJobImagesList(jobIdList);

		HashMap<Integer,List<String>> jobImagesMap = new HashMap<Integer,List<String>>();
		List<String> imagesList = new ArrayList<String>();

		if(null != docDetailsList) {
			for(DocumentDetails dd : docDetailsList) {
				if(jobImagesMap.containsKey(dd.getJobId())) imagesList = jobImagesMap.get(dd.getJobId());
				else imagesList = new ArrayList<String>();
				imagesList.add(dd.getDocUrl());
				jobImagesMap.put(dd.getJobId(),imagesList);
			}
		}

		int index=0;
		for(JobHistoryVO jh : jhList) {
			if(jobImagesMap.containsKey(jh.getJobId())) {
				jh.setDocUrl(jobImagesMap.get(jh.getJobId()));
				jhList.set(index,jh);	
			}
			index++;
		}
		
		return jobHistoryList;
	}

	@Override
	public List<RatingVO> getJobFeedbackTotalRating() {
		return jobDataMapper.getJobFeedbackTotalRating();
	}

	@Override
	public HashMap<Integer,Jobs> getFavoriteJobs(int userId) {
		List<Jobs> favJobs = jobDataMapper.getFavoriteJobs(userId);
		HashMap<Integer,Jobs> jobMap = new HashMap<Integer,Jobs>();
		for(Jobs j : favJobs) jobMap.put(j.getJobId(), j);
		return jobMap;
	}

	@Override
	public HashMap<Integer,List<String>> getJobImagesList(List<Integer> jobIdList) {
		
		HashMap<Integer,List<String>> jobImagesMap = new HashMap<Integer,List<String>>();
		List<DocumentDetails> docDetailsList = null;
		if(null != jobIdList && jobIdList.size()>0) docDetailsList = jobDataMapper.getJobImagesList(jobIdList);
		List<String> imagesList;
		if(null != docDetailsList) {
			for(DocumentDetails dd : docDetailsList) {
				if(jobImagesMap.containsKey(dd.getJobId())) imagesList = jobImagesMap.get(dd.getJobId());
				else imagesList = new ArrayList<String>();
				imagesList.add(dd.getDocUrl());
				jobImagesMap.put(dd.getJobId(),imagesList);
			}
		}		
		return jobImagesMap;
	}
	@Override
	public int jobLogRecordCount(int userId, int jobId, String logType) {

		int count = jobDataMapper.getJobLogRecordCount(userId,jobId,logType);
		return count;

	}

	@Override
	public Integer getJobId(int userId, int orgId, Date curDate) {

		Integer jobId = jobDataMapper.getJobIdForUserByOrgId(userId,orgId, new java.util.Date());
	    return jobId;

	}


	@Override
	public Instant getApplicationDeadline(int jobId ) {

		Instant deadline = jobDataMapper.getApplicationDeadlineByJobId(jobId);
		return deadline;
	}

	@Override
	public int getApplication(int jobId, int userId) {


		 int applicationId = jobDataMapper.getJobApplicationById(jobId,userId);
		 return applicationId;
	}
	}


