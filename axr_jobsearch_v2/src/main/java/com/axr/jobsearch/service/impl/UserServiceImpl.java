package com.axr.jobsearch.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.common.exception.DocumentException;
import com.axr.jobsearch.dataaccess.email.EmailDataMapper;
import com.axr.jobsearch.dataaccess.model.email.EmailAttribute;
import com.axr.jobsearch.dataaccess.model.email.EmailMessage;
import com.axr.jobsearch.dataaccess.model.email.EmailTemplate;
import com.axr.jobsearch.dataaccess.model.users.DocumentDetails;
import com.axr.jobsearch.dataaccess.model.users.Languages;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.model.users.ValidationCodeDetails;
import com.axr.jobsearch.dataaccess.users.UserDataMapper;
import com.axr.jobsearch.dataaccess.users.valueobjects.UserProfileVO;
import com.axr.jobsearch.service.EmailService;
import com.axr.jobsearch.service.UserService;
import com.axr.jobsearch.utils.ConfigMap;
import com.axr.jobsearch.utils.EmailUtility;

@Service
public class UserServiceImpl implements UserService {
	
	static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDataMapper userDataMapper;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private EmailDataMapper emailDataMapper;

	@Override
	public UserProfile addUser(UserProfileVO userProfileVO) {

		
		UserProfile userProfile = new UserProfile();
		userProfile.setFirstName(userProfileVO.getFirstName());
		userProfile.setLastName(userProfileVO.getLastName());
		userProfile.setKanaFirstName(userProfileVO.getKanaFirstName());
		userProfile.setKanaLastName(userProfileVO.getKanaLastName());
		userProfile.setEmailAddress(userProfileVO.getEmailAddress());
		userProfile.setPassword(userProfileVO.getPassword());
		userProfile.setGender(userProfileVO.getGender());
		userProfile.setPrefectureId(userProfileVO.getPrefectureId());
		userProfile.setDateOfBirth(userProfileVO.getDateOfBirth());
		userProfile.setMobileNumber(userProfileVO.getMobileNumber());
		
		/***
		 * Set defaults. 
		 * Currently only job seekers are allowed to register via this. 
		 * For other type of users like org users, admin users, the registration will be done from backend. 
		 * In future this can be enhanced to add via API
		 */
		//TODO: Use enum or other approach instead of hard coding the value
		userProfile.setRoleId(Constants.DEFAULT_USER_ROLE_ID);
		userProfile.setPreferedLanguage(Constants.DEFAULT_USER_LANGUAGE_ID);
		userProfile.setUserTypeId(Constants.DEFAULT_USER_TYPE_ID);
		
		userProfile.setIsActive(Constants.STATUS_NO);
		userProfile.setStatus(Constants.USER_STATUS_PENDING);
		
		userProfile.setCreatedDate(Instant.now());
		userProfile.setLastAccessedDate(Instant.now());
		userProfile.setLastUpdatedDate(Instant.now());
		
		userDataMapper.addUserProfile(userProfile);
		
		
		return userProfile;
	}

	@Override
	public void delete(long userId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UserProfile updateUser(UserProfileVO userProfileVO) {
		
		UserProfile userProfile = userDataMapper.getUserProfile(userProfileVO.getEmailAddress());
		
		
		if(null != userProfileVO.getFirstName() && !"".equals(userProfileVO.getFirstName())) userProfile.setFirstName(userProfileVO.getFirstName());
		if(null != userProfileVO.getLastName() && !"".equals(userProfileVO.getLastName())) userProfile.setLastName(userProfileVO.getLastName());
		if(null != userProfileVO.getKanaFirstName() && !"".equals(userProfileVO.getKanaFirstName())) userProfile.setKanaFirstName(userProfileVO.getKanaFirstName());
		if(null != userProfileVO.getKanaLastName() && !"".equals(userProfileVO.getKanaLastName())) userProfile.setKanaLastName(userProfileVO.getKanaLastName());
		if(null != userProfileVO.getPassword() && !"".equals(userProfileVO.getPassword())) userProfile.setPassword(userProfileVO.getPassword());
		if(null != userProfileVO.getGender() && !"".equals(userProfileVO.getGender())) 	userProfile.setGender(userProfileVO.getGender());
		if(userProfileVO.getPrefectureId() > 0) userProfile.setPrefectureId(userProfileVO.getPrefectureId());
		if(null != userProfileVO.getDateOfBirth()) userProfile.setDateOfBirth(userProfileVO.getDateOfBirth());
		if(null != userProfileVO.getMobileNumber() && !"".equals(userProfileVO.getMobileNumber())) userProfile.setMobileNumber(userProfileVO.getMobileNumber());
		userProfile.setLastUpdatedDate(Instant.now());
		
		try {
			userDataMapper.updateUserProfile(userProfile);
		}catch(Exception ee) {
			logger.error("Error while updating uder details",ee);
		}
		logger.info("\n\n****** The user record updated for user -->"+userProfile.toString()+"<----\n\n");
		
		return userDataMapper.getUserProfile(userProfileVO.getEmailAddress());
	}

	@Override
	public UserProfile registerUser(UserProfile userProfile) {
		
		userProfile.setRoleId(Constants.DEFAULT_USER_ROLE_ID);
		userProfile.setPreferedLanguage(Constants.DEFAULT_USER_LANGUAGE_ID);
		userProfile.setUserTypeId(Constants.DEFAULT_USER_TYPE_ID);
		
		userProfile.setIsActive(Constants.STATUS_NO);
		userProfile.setStatus(Constants.USER_STATUS_PENDING);
		userProfile.setCreatedDate(Instant.now());
		userProfile.setLastAccessedDate(Instant.now());
		userProfile.setLastUpdatedDate(Instant.now());
		
		userDataMapper.addUserProfile(userProfile);
		
		EmailMessage emailMessage = new EmailMessage();
		
		//HashMap<String,HashMap<String,String>> configMap = utilityService.getConfigParameters();
		HashMap<String,String> emailConfigMap = ConfigMap.getInstance().getConfigCategoryMap().get(Constants.EMAIL);
		UUID uuid = UUID.randomUUID();
		
		emailMessage.setEmailFrom(emailConfigMap.get(Constants.EMAIL_FROM));
		emailMessage.setEmailTo(userProfile.getEmailAddress());
		emailMessage.setEmailBcc("");
		emailMessage.setEmailCC("");
		//TODO: Use Constants or ConfigMap param to get subject value &language 
		emailMessage.setSubject(emailConfigMap.get("invitation.email.subject"));
		emailMessage.setContentType(emailConfigMap.get("invitation.email.content.type"));
		emailMessage.setCharset(emailConfigMap.get("invitation.email.charset"));
		emailMessage.setStatus("");
		emailMessage.setRetryCount(Integer.parseInt(emailConfigMap.get("mail.retry.count")));
		emailMessage.setRemarks("");
		emailMessage.setEmailReplyTo(emailConfigMap.get(Constants.EMAIL_FROM));
		emailMessage.setCreatedDate(Instant.now());
		emailMessage.setLastUpdatedDate(Instant.now());
		EmailTemplate emailTemplate = emailService.getEmailTemplate("INVITE_USER");
		emailMessage.setEmailTemplate(emailTemplate);
		emailMessage.setTemplateId(emailTemplate.getTemplateId());
		
		emailDataMapper.saveEmailMessage(emailMessage);
		
		ArrayList<EmailAttribute> emailAttributeList = new ArrayList<EmailAttribute>();
		
		EmailAttribute emailAttribute = new EmailAttribute();
		emailAttribute.setAttributeName(Constants.FIRST_NAME);
		emailAttribute.setAttributeValue(userProfile.getFirstName());	
		emailAttribute.setMessageId(emailMessage.getMessageId());
		emailAttributeList.add(emailAttribute);
		
		emailAttribute = new EmailAttribute();
		emailAttribute.setAttributeName(Constants.LAST_NAME);
		emailAttribute.setAttributeValue(userProfile.getLastName());
		emailAttribute.setMessageId(emailMessage.getMessageId());
		emailAttributeList.add(emailAttribute);
		
		emailAttribute = new EmailAttribute();
		emailAttribute.setAttributeName(Constants.ACTIVATION_LINK);		
		emailAttribute.setAttributeValue(emailConfigMap.get(Constants.ACTIVATION_LINK)+uuid);
		emailAttribute.setMessageId(emailMessage.getMessageId());
		emailAttributeList.add(emailAttribute);
		
		emailMessage.setEmailAttributeList(emailAttributeList);

		for(EmailAttribute ea : emailAttributeList) {
			emailDataMapper.saveEmailAttribute(ea);
		}
		
		ValidationCodeDetails validationCodeDetails = new ValidationCodeDetails();
		validationCodeDetails.setValidationCategory(Constants.VALIDATION_CATEGORY_NEW_USER);
		validationCodeDetails.setUserId(userProfile.getUserId());

		validationCodeDetails.setCodeInfo(""+uuid);
		validationCodeDetails.setValidTill(Instant.now().plusSeconds(Constants.DEFAULT_USER_ACTIVATION_CODE_VALIDITY_IN_HOURS*60*60));
		validationCodeDetails.setEmailMessageId(emailMessage.getMessageId());
		validationCodeDetails.setValidationStatus(Constants.PENDING);
		//emailService.sendUserInviteEmail(userProfile);
		
		userDataMapper.addVerificationCode(validationCodeDetails);
		emailService.sendEmail(emailMessage);
		return userProfile;
	}
	
 

	@Override
	public String activateUser(String validationCode) {
		
		String result="";
		
		ValidationCodeDetails validationCodeDetails = userDataMapper.getValidationCodeDetails(validationCode);
		if(validationCodeDetails.getValidTill().isAfter(Instant.now())) {
			if(validationCodeDetails.getValidationStatus().equalsIgnoreCase(Constants.PENDING)) {
				userDataMapper.updateValidationStatus(Constants.SUCCESS, validationCode,Instant.now());
				userDataMapper.updateUserStatus(Constants.ACTIVE, Constants.STATUS_YES, validationCodeDetails.getUserId(), Instant.now());
				result = "Your account is successfuly verified. Please login to our application with your credentials.";
			}else result = "Your account was verified earlier.";
		}else result = "The activation link is expired. Please use forgot password option to get new link.";
		return result;
	}

	@Override
	public List<Prefectures> getPrefectures(int countryId) {
		
		return userDataMapper.getPrefectures(countryId);
	}

	@Override
	public String forgotPassword(String emailAddress) {
		
		String message = "";
		UserProfile userProfile = userDataMapper.getUserProfile(emailAddress);
		HashMap<String,String> emailConfigMap = ConfigMap.getInstance().getConfigCategoryMap().get(Constants.EMAIL);
		//TODO: To avoid user enumeration attacks, bring captcha validations & provide generic message
		//if(null == userProfile) message = "The email address doesn't exist in the system";
		if(null == userProfile) message = " EmailId is not registered";
		else {
			//TODO: There is a possiblity of same uuid is getting regenerated. Since there ia unique key constraint in DB for this, handle that exception & re-generate uuid or add some additional prefix to avoid like System.currentTimeInMillis
			UUID uuid = UUID.randomUUID();
			EmailMessage emailMessage = new EmailMessage();
			ValidationCodeDetails validationCodeDetails = new ValidationCodeDetails();
			
			if(userProfile.getIsActive().equalsIgnoreCase(Constants.STATUS_YES) && userProfile.getStatus().equalsIgnoreCase(Constants.ACTIVE)) {
				//Forgot password code
				
				emailMessage.setEmailFrom(emailConfigMap.get(Constants.EMAIL_FROM));
				emailMessage.setEmailTo(userProfile.getEmailAddress());
				emailMessage.setEmailBcc("");
				emailMessage.setEmailCC("");
				//TODO: Use Constants or ConfigMap param to get subject value &language 
				emailMessage.setSubject(emailConfigMap.get("forgotpassword.email.subject"));
				emailMessage.setContentType(emailConfigMap.get("forgotpassword.email.content.type"));
				emailMessage.setCharset(emailConfigMap.get("forgotpassword.email.charset"));
				emailMessage.setStatus("");
				emailMessage.setRetryCount(Integer.parseInt(emailConfigMap.get("mail.retry.count")));
				emailMessage.setRemarks("");
				emailMessage.setEmailReplyTo(Constants.EMAIL_FROM);
				emailMessage.setCreatedDate(Instant.now());
				emailMessage.setLastUpdatedDate(Instant.now());
				EmailTemplate emailTemplate = emailService.getEmailTemplate("FORGOT_PASSWORD");
				emailMessage.setEmailTemplate(emailTemplate);
				emailMessage.setTemplateId(emailTemplate.getTemplateId());
				
				emailDataMapper.saveEmailMessage(emailMessage);
				
				ArrayList<EmailAttribute> emailAttributeList = new ArrayList<EmailAttribute>();
				
				EmailAttribute emailAttribute = new EmailAttribute();
				emailAttribute.setAttributeName(Constants.FIRST_NAME);
				emailAttribute.setAttributeValue(userProfile.getFirstName());	
				emailAttribute.setMessageId(emailMessage.getMessageId());
				emailAttributeList.add(emailAttribute);
				
				emailAttribute = new EmailAttribute();
				emailAttribute.setAttributeName(Constants.LAST_NAME);
				emailAttribute.setAttributeValue(userProfile.getLastName());
				emailAttribute.setMessageId(emailMessage.getMessageId());
				emailAttributeList.add(emailAttribute);
				
				emailAttribute = new EmailAttribute();
				emailAttribute.setAttributeName(Constants.RESET_PASSWORD_LINK);		
				emailAttribute.setAttributeValue(emailConfigMap.get(Constants.RESET_PASSWORD_LINK)+uuid);
				emailAttribute.setMessageId(emailMessage.getMessageId());
				emailAttributeList.add(emailAttribute);
				
				emailMessage.setEmailAttributeList(emailAttributeList);

				for(EmailAttribute ea : emailAttributeList) {
					emailDataMapper.saveEmailAttribute(ea);
				}

				
				validationCodeDetails.setValidationCategory(Constants.VALIDATION_CATEGORY_FORGOT_PASSWORD);
				validationCodeDetails.setUserId(userProfile.getUserId());
				validationCodeDetails.setCodeInfo(""+uuid);
				validationCodeDetails.setValidTill(Instant.now().plusSeconds(Constants.DEFAULT_USER_FORGOT_PASSWORD_CODE_VALIDITY_IN_HOURS*60*60));
				validationCodeDetails.setEmailMessageId(emailMessage.getMessageId());
				validationCodeDetails.setValidationStatus(Constants.PENDING);
				
			}else {
				//User activation code
				
				emailMessage.setEmailFrom(emailConfigMap.get(Constants.EMAIL_FROM));
				emailMessage.setEmailTo(userProfile.getEmailAddress());
				emailMessage.setEmailBcc("");
				emailMessage.setEmailCC("");
				//TODO: Use Constants or ConfigMap param to get subject value &language 
				emailMessage.setSubject(emailConfigMap.get("invitation.email.subject"));
				emailMessage.setContentType(emailConfigMap.get("invitation.email.content.type"));
				emailMessage.setCharset(emailConfigMap.get("invitation.email.charset"));
				emailMessage.setStatus("");
				emailMessage.setRetryCount(Integer.parseInt(emailConfigMap.get("mail.retry.count")));
				emailMessage.setRemarks("");
				emailMessage.setEmailReplyTo(emailConfigMap.get(Constants.EMAIL_FROM));
				emailMessage.setCreatedDate(Instant.now());
				emailMessage.setLastUpdatedDate(Instant.now());
				EmailTemplate emailTemplate = emailService.getEmailTemplate("INVITE_USER");
				emailMessage.setEmailTemplate(emailTemplate);
				emailMessage.setTemplateId(emailTemplate.getTemplateId());
				
				emailDataMapper.saveEmailMessage(emailMessage);
				
				ArrayList<EmailAttribute> emailAttributeList = new ArrayList<EmailAttribute>();
				
				EmailAttribute emailAttribute = new EmailAttribute();
				emailAttribute.setAttributeName(Constants.FIRST_NAME);
				emailAttribute.setAttributeValue(userProfile.getFirstName());	
				emailAttribute.setMessageId(emailMessage.getMessageId());
				emailAttributeList.add(emailAttribute);
				
				emailAttribute = new EmailAttribute();
				emailAttribute.setAttributeName(Constants.LAST_NAME);
				emailAttribute.setAttributeValue(userProfile.getLastName());
				emailAttribute.setMessageId(emailMessage.getMessageId());
				emailAttributeList.add(emailAttribute);
				
				emailAttribute = new EmailAttribute();
				emailAttribute.setAttributeName(Constants.ACTIVATION_LINK);		
				emailAttribute.setAttributeValue(emailConfigMap.get(Constants.ACTIVATION_LINK)+uuid);
				emailAttribute.setMessageId(emailMessage.getMessageId());
				emailAttributeList.add(emailAttribute);
				
				emailMessage.setEmailAttributeList(emailAttributeList);

				for(EmailAttribute ea : emailAttributeList) {
					emailDataMapper.saveEmailAttribute(ea);
				}
				
				validationCodeDetails.setValidationCategory(Constants.VALIDATION_CATEGORY_NEW_USER);
				validationCodeDetails.setUserId(userProfile.getUserId());

				validationCodeDetails.setCodeInfo(""+uuid);
				validationCodeDetails.setValidTill(Instant.now().plusSeconds(Constants.DEFAULT_USER_ACTIVATION_CODE_VALIDITY_IN_HOURS*60*60));
				validationCodeDetails.setEmailMessageId(emailMessage.getMessageId());
				validationCodeDetails.setValidationStatus(Constants.PENDING);
			
			}
			
			userDataMapper.addVerificationCode(validationCodeDetails);
			emailService.sendEmail(emailMessage);
			
		//TODO: Can this be a common message??
			message = " Link to reset the password has been sent to your EmailId";
		
		}
				
		return message;
	}

	@Override
	public String resetPasswordToken(String validationCode) {
		String result = "";
		//TODO: Use config to redirect to the page
		String message = "<html><body><form name='resetPassword' action='/jobsearch/api/v1/resetPassword' method='POST'>"
				+ "Password <input type='password' name='password' value=''/><br><br>"
				+ "Confirm Password <input type='password' name='confirmPassword' value=''/><br><br>"
				+ "<input type='hidden' name='verificationCode' value='"+validationCode+"'/>"
				+ "<input type='submit' name='submit' value='Submit'/>"
				+ "</form></body></html";
		ValidationCodeDetails validationCodeDetails = userDataMapper.getValidationCodeDetails(validationCode);
		if(validationCodeDetails.getValidTill().isAfter(Instant.now())) {
			if(validationCodeDetails.getValidationStatus().equalsIgnoreCase(Constants.PENDING)) {
				//+userDataMapper.updateValidationStatus(Constants.SUCCESS, token,Instant.now());
				result = message;
			}else result = "The password reset was completed earlier using this code.";
		}else result = "The link is expired. Please use forgot password option to get a new link.";
		
		return result;
	}

	@Override
	public String resetPassword(String validationCode, String password) {
	
		String result="";
		
		ValidationCodeDetails validationCodeDetails = userDataMapper.getValidationCodeDetails(validationCode);
		
		if(validationCodeDetails.getValidTill().isAfter(Instant.now())) {
			if(validationCodeDetails.getValidationStatus().equalsIgnoreCase(Constants.PENDING)) {
				userDataMapper.updateValidationStatus(Constants.SUCCESS, validationCode,Instant.now());
				userDataMapper.updateUserPassword(validationCodeDetails.getUserId(), password, Instant.now());
				result = "Your password is updated successfuly. Please login to our application with your credentials.";
			}else result = "The password reset was completed earlier using this code.";
		}else result = "The link is expired. Please use forgot password option to get a new link.";
		
		return result;
	}

	@Override
	public UserProfile retriveUser(int userId) {
		UserProfile userProfile = userDataMapper.getUserProfileById(userId);
		DocumentDetails docDetails = userDataMapper.getDocumentsByUserId(userId);
		if(null != docDetails) userProfile.setDocUrl(docDetails.getDocUrl());
		return userProfile;
	}

	@Override
	public List<Languages> getLanguages() {
		return userDataMapper.getLanguages();
	}

	@Override
	public String updateLanguage(int userId, int languageId) {
		//TODO: Handle error/failure to update scenario
		userDataMapper.updateLanguage(userId,languageId);
		return "Updated the record";
	}

	@Override
	public String axrLogout(int userId, String token) {		
		userDataMapper.axrLogout(userId, token);
		return "Successfully logged out";
	}

	@Override
	public DocumentDetails saveDocument(MultipartFile file, int docCategoryId, int userId, Integer orgId, Integer jobId) {
		
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		String newFileName = "";
		String fileExtension="";
		Path storePath = null;
		
		Integer oorgId = null;
		Integer jjobId = null;
		String docType = "application/octet-stream";
		DocumentDetails documentDetails = null;
		
		try {
			
			storePath = Paths.get(ConfigMap.getInstance().getConfigCategoryMap().get(Constants.DOCUMENTS).get(Constants.FILE_STORE_PATH));
			
			if(fileName.contains("..")) throw new DocumentException("The document contains invalid path sequence: "+fileName);
			fileExtension = fileName.substring(fileName.lastIndexOf(".")+1);
			newFileName = userId+"_"+docCategoryId+Constants.DOT+fileExtension;
			
			Path targetLocation = storePath.resolve(newFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
					
            documentDetails = new DocumentDetails();
            documentDetails.setDocName(newFileName);
            documentDetails.setDocPath(targetLocation.toString());
            String docUrl = Constants.ASSETS+newFileName;
            documentDetails.setDocUrl(docUrl);
            
            
            //TODO: Should use configurable method to dynamically add/update the mime-types
            if(fileExtension.equalsIgnoreCase("pdf")) docType = "application/pdf";
            else if(fileExtension.equalsIgnoreCase("png")) docType = "image/png";
            else if(fileExtension.equalsIgnoreCase("jpeg") || fileExtension.equalsIgnoreCase("jpg")) docType = "image/jpeg";
            else throw new DocumentException("Unsupported type of file: "+fileName +" Supports only [pdf, jpg/jpeg, png]");
               
            
            documentDetails.setDocCategoryId(docCategoryId);
            documentDetails.setUserId(userId);
            
            //TODO: avoid relying on primary key values
            if(docCategoryId<=5) {
            	documentDetails.setUserId(userId); 
            }
            else if(docCategoryId>5 && docCategoryId<=7) {
            	if(null != orgId && orgId>0) oorgId= orgId;
            	else throw new DocumentException(" Invalid orgId for this document category");
            }
            else if(docCategoryId == 8) {
            	if(null != jobId && jobId>0) jjobId = jobId;
            	else throw new DocumentException(" Invalid jobId for this document category");
            	documentDetails.setOrgId((Integer) null);
            }
          
            documentDetails = userDataMapper.getDocuments(docCategoryId, userId);
            
            if(null == documentDetails) userDataMapper.addDocuments(newFileName, targetLocation.toString(), docType, docCategoryId, userId, oorgId, jjobId, docUrl);
            else userDataMapper.updateDocuments(documentDetails.getDocId(), newFileName, targetLocation.toString(), docType, docCategoryId, userId, oorgId, jjobId, docUrl);
            documentDetails = userDataMapper.getDocuments(docCategoryId, userId);
            
		} catch (DocumentException ex) {
			logger.error("Error while storing the document:: "+fileName+" userId: "+userId+" orgId: "+orgId+" jobId: "+jobId,ex);
            throw new DocumentException("Invalid input [jobId|orgId] for this document category");
        } catch (Exception ex) {
        	logger.error("Error while storing the document: "+fileName+" userId: "+userId+" orgId: "+orgId+" jobId: "+jobId,ex);
            throw new DocumentException("Could not store document " + fileName + ". Please try again!");
        }
		
		return documentDetails;
	}

	@Override
	public DocumentDetails getDocument(int docId, int userId) {
		
		return userDataMapper.getDocumentsByDocId(docId, userId);
	}
         @Override 
          public DocumentDetails getDocumentByUserId(int userId) {
	   
	  return userDataMapper.getDocumentsByUserId(userId);
   }
	

}
