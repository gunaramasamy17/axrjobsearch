package com.axr.jobsearch.service.impl;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.dataaccess.jobs.JobDataMapper;
import com.axr.jobsearch.dataaccess.model.notifications.Notifications;
import com.axr.jobsearch.dataaccess.notifications.NotificationDataMapper;
import com.axr.jobsearch.service.NotificationService;
@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	NotificationDataMapper notificationDataMapper;

	@Autowired
	JobDataMapper jobDataMapper;
	
	

	@Override
	public List<Notifications> getUserNotification(int userId) {
		return notificationDataMapper.getNotificationsByUserId(userId);
	}

	@Override
	public Notifications getNotificationById(int notificationId) {
		Notifications notification = new Notifications();
		notification = notificationDataMapper.getNotificatonById(notificationId);
		notificationDataMapper.updateStatus(notificationId);
		return notification;
	}

	

}
