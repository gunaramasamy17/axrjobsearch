package com.axr.jobsearch.service.users.impl;

import java.util.Optional;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.common.exception.LoginAuthenticationException;
import com.axr.jobsearch.dataaccess.model.users.AuthToken;
import com.axr.jobsearch.dataaccess.model.users.Login;
import com.axr.jobsearch.dataaccess.users.UserDataMapper;
import com.axr.jobsearch.service.users.AuthenticationService;
import com.axr.jobsearch.utils.ConfigMap;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.model.users.UserStatus;
import com.axr.jobsearch.dataaccess.model.utils.UserAccessToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	
	static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
	
	@Autowired
	UserDataMapper userDataMapper;	
	
	public static final String TYPE = "type";
	public static final String REFRESH = "refresh";
	

	/**
	 * authenticate the user
	 * 
	 * @param login
	 *            - user loginame and password
	 * @return AuthToken - user tokens
	 */
	@Override
	public AuthToken login(Login login) {
		AuthToken authToken = new AuthToken();
		String loginName = login.getUsername();
		
		
	
		UserProfile userProfile = userDataMapper.getUserProfile(loginName);

		
		if(null == userProfile) {
			throw new LoginAuthenticationException("Username or password is incorrect",new Exception("Username or password is incorrect"));
		}else {
			if(!userProfile.getStatus().equalsIgnoreCase(UserStatus.ACTIVE.value())) {
				throw new AccessDeniedException("The account is not active. Please verify your email.");
			}else {
				//if(BCrypt.checkpw(login.getPassword(), userProfile.getPassword())) {
				if(login.getPassword().equals(userProfile.getPassword())) {
					authToken = generateAuthToken(userProfile);
					//fillPrivileges(userProfile.get().getRole());
				}
			}
		}
		
				
		
		return authToken;
	}
	
	/**
	 * method to generate auth token
	 * 
	 * @param login
	 *            - user loginame and password
	 * @return AuthToken - user tokens
	 */
	private AuthToken generateAuthToken(UserProfile userProfile) {
		AuthToken authToken = new AuthToken();
		
		UserAccessToken userAccessToken = new UserAccessToken();
		
		String jti = UUID.randomUUID().toString();
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[16];
		random.nextBytes(bytes);
		String secretKey = Base64.getEncoder().encodeToString(bytes);
		String accessToken = generateAccessToken(secretKey, jti);
		String refreshToken = generateRefreshToken(secretKey, jti);
		userAccessToken.setJti(jti);
		userAccessToken.setSecretKey(secretKey);
		userAccessToken.setToken(accessToken);
		userAccessToken.setUserId(userProfile.getUserId());
		userAccessToken.setCreatedDate(Instant.now());
		userAccessToken.setLastAccessedDate(Instant.now());
		userDataMapper.saveUserAccessToken(userAccessToken);
		authToken.setAccessToken(accessToken);
		authToken.setRefreshToken(refreshToken);
		logger.info("successfully generated and persisted user tokens");
		return authToken;
	}
	
	public String generateAccessToken(String secret , String jti) {
		Claims claims = Jwts.claims();
		claims.put(Constants.JTI, jti);
		return Jwts.builder().setClaims(claims).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + (Long.parseLong(ConfigMap.getInstance().getConfigCategoryMap().get(Constants.TOKEN).get(Constants.ACCESS_TOKEN_EXPIRY))*1000) ))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}
	
	public String generateRefreshToken(String secret , String jti) {
		Claims claims = Jwts.claims();
		claims.put(Constants.JTI, jti);
		claims.put(TYPE, REFRESH);
		return Jwts.builder().setClaims(claims).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + (Long.parseLong(ConfigMap.getInstance().getConfigCategoryMap().get(Constants.TOKEN).get(Constants.REFRESH_TOKEN_EXPIRY))*1000)))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}

}
