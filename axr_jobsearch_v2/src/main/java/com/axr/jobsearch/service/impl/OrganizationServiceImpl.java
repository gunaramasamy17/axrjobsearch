package com.axr.jobsearch.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.organization.Organization;
import com.axr.jobsearch.dataaccess.organization.OrganizationDataMapper;
import com.axr.jobsearch.service.OrganizationService;

@Service
public class OrganizationServiceImpl implements OrganizationService {
	
	static final Logger logger = LoggerFactory.getLogger(OrganizationServiceImpl.class);
	
	@Autowired
	private OrganizationDataMapper organizationDataMapper;
	
	@Override
	public Organization getOrganizationInfoByOrgId(int orgId) {
		
		return organizationDataMapper.getOrganizationInfoByOrgId(orgId);
		
	}

}
