package com.axr.jobsearch.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.axr.jobsearch.dataaccess.model.utils.ConfigParams;
import com.axr.jobsearch.dataaccess.utils.UtilityDataMapper;
import com.axr.jobsearch.service.UtilityService;

@Service
public class UtilityServiceImpl implements UtilityService {

	@Autowired
	private UtilityDataMapper utilityDataMapper;
	
	@Override
	public HashMap<String, HashMap<String,String>> getConfigParameters() {

		List<ConfigParams> configParamList = utilityDataMapper.getAllConfigParams();
		
		HashMap<String, HashMap<String,String>> configCategoryMap = new HashMap<String, HashMap<String,String>>();
		HashMap<String,String> configMap = null;
		
		for(ConfigParams cp : configParamList) {
			if(configCategoryMap.containsKey(cp.getParamCategory())) {
				configMap = configCategoryMap.get(cp.getParamCategory());
			}else configMap = new HashMap<String,String>();
			
			configMap.put(cp.getParamName(), cp.getParamValue());
			configCategoryMap.put(cp.getParamCategory(), configMap);
		}
		
		return configCategoryMap;
	}

}
