package com.axr.jobsearch.dataaccess.model.organization;

import java.time.Instant;
import java.util.Date;

import com.axr.jobsearch.dataaccess.model.users.UserProfile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Organization {
	
	private int orgId;
	private String orgName;
	private String orgDescription ;
	private String orgCategory;
	private String contactDetails;
	private String addressDetails; 
	private int prefectureId;
	private int imageId ;
	private char isActive ;
	private String status ;
	private Instant createdDate;
	private int createdBy ;
	private Instant lastUpdatedDate ;
	private int lastUpdatedBy ;
	private String googleMapUrl;
	

}
