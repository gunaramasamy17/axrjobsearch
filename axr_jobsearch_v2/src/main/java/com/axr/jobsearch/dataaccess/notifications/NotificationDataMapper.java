package com.axr.jobsearch.dataaccess.notifications;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.axr.jobsearch.dataaccess.model.notifications.Notifications;
@Mapper
public interface NotificationDataMapper {
    


	@Select("Select * from notifications where user_id=#{userId}")
	List<Notifications> getUserNotification(int userId);

	@Select("Select * from notifications where notification_Id=#{notificationId}")
	Notifications getNotificatonById(int notificationId);
    
	@Update("update notifications set notification_status='Read' where notification_id=#{notificationId} ")
	void updateStatus(int notificationId);
    
	
	@Select("Select  n.notification_id, n.job_id,n.user_id, n.notification_message, n.notification_category, n.notification_status, n.created_date, n.last_updated_date,j.job_title from notifications n,job_details j where n.user_id=#{userId} and j.start_time > current_time() and n.job_id=j.job_id ")
	List<Notifications> getNotificationsByUserId(int userId);

	@Select("Select COUNT(*) from notifications where user_id=#{userId} and job_id=#{jobId} and notification_category='Invite'")
	int getInvitedUsersCount(int userId, int jobId);

}

