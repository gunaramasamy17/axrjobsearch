package com.axr.jobsearch.dataaccess.skills;

import java.time.Instant;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.axr.jobsearch.dataaccess.model.jobs.Favorites;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.SkillDetails;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.model.users.ValidationCodeDetails;

@Mapper
public interface SkillDataMapper {


	@Select("SELECT * FROM skill_details")
	List<SkillDetails> retriveAllSkills();
	
	@Select("SELECT * FROM skill_details where skill_id in (select skill_id from user_skill_map where user_id = #{userId})")
	List<SkillDetails> retriveSkillsByUserId(int userId);
	
	@Delete("DELETE FROM user_skill_map WHERE user_id=#{userId}")
	void deleteUserSkills(int userId);
	
	@Insert("INSERT INTO user_skill_map(user_id,skill_id) values(#{userId},#{skillId})")
	int addUserSkills(int userId, int skillId);
}
