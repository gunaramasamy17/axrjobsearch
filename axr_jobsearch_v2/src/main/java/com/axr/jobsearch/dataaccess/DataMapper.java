package com.axr.jobsearch.dataaccess;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.session.RowBounds;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import org.springframework.context.annotation.Bean;


@Mapper
public interface DataMapper {

	@Select("SELECT * FROM country")
	List<Country> getCountryList();
	
	@Insert("INSERT into country (country_id, country_code, country_name) values (#{countryId},#{countryCode},#{countryName})")
	Long insertCountry(Country country);
}
