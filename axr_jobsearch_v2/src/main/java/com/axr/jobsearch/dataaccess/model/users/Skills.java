package com.axr.jobsearch.dataaccess.model.users;

import java.time.Instant;

import com.axr.jobsearch.dataaccess.model.jobs.Favorites;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Skills {

	private int skillId;
	private String skillName;
	private String description;
	private String category;
}
