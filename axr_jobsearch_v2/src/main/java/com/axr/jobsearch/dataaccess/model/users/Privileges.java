package com.axr.jobsearch.dataaccess.model.users;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Privileges {

	private int privilegeId;
	private String privilegeName;
	private String remarks;
}
