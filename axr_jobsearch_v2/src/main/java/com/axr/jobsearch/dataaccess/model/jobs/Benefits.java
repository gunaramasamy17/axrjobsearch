package com.axr.jobsearch.dataaccess.model.jobs;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Benefits {
  private int benefitId;
  private String benefitName;
  private int docId;
  private String docUrl;
}
