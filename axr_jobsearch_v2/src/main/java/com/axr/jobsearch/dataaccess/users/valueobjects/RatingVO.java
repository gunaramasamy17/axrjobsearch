package com.axr.jobsearch.dataaccess.users.valueobjects;

import java.time.Instant;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RatingVO {

	private int orgId;
	private float totalRating;
}
