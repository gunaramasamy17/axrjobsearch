package com.axr.jobsearch.dataaccess.model.users;

import java.time.Instant;
import java.util.Date;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserProfile {

	private int userId;	
	private String firstName;
	private String lastName;
	private String kanaFirstName;
	private String kanaLastName;
	private Date dateOfBirth;
	private String mobileNumber;
	private String emailAddress;
	private String password;
	private String gender;
	private int prefectureId;
	private int roleId;
	private int preferedLanguage;
	private int userTypeId;
	private String isActive;
	private String status;
	private Instant createdDate;
	private Instant lastUpdatedDate;
	private Instant lastAccessedDate;
	private String docUrl;

	
}
