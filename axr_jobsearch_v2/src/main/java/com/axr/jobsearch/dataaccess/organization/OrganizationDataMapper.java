package com.axr.jobsearch.dataaccess.organization;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.axr.jobsearch.dataaccess.model.organization.Organization;

@Mapper
public interface OrganizationDataMapper {
	
	
	@Select("SELECT*FROM organization where org_id=#{orgId}")
	Organization getOrganizationInfoByOrgId(int orgId);
	
	
	

}
