package com.axr.jobsearch.dataaccess.users.valueobjects;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobHistoryVO {

	private int jobId;
	private Instant startTime;
	private Instant endTime;
	private float fixedRate;
	private String jobTitle;
	private String feedbackDescription;
	private boolean penaltyApplied;
	private float penaltyCost;	
	private List<String> docUrl;
}
