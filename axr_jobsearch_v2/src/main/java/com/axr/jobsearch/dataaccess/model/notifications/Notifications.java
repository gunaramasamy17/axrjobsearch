package com.axr.jobsearch.dataaccess.model.notifications;

import java.time.Instant;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Notifications {
	//notification_id, user_id, job_id, notification_message, notification_category, notification_status, created_date, last_updated_date

	private int notificationId;
	private int userId;
	private int jobId;
	private String notificationMessage;
	private String notificationCategory;
	private String notificationStatus;
	private Instant createdDate;
	private Instant lastUpdatedDate;
}
