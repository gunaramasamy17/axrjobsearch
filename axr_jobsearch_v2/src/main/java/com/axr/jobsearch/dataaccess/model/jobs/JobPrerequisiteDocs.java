package com.axr.jobsearch.dataaccess.model.jobs;

import java.time.Instant;

import com.axr.jobsearch.dataaccess.users.valueobjects.JobsVO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobPrerequisiteDocs {

	private int jobId;
	private int docCategoryId;
	private String docCategory;
}
