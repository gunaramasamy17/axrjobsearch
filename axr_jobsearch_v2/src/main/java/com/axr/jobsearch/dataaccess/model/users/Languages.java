package com.axr.jobsearch.dataaccess.model.users;

import java.time.Instant;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Languages {

	private int languageId;
	private String languageName;
	private String languageCode;
	private String remarks;
	
}
