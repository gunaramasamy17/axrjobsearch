package com.axr.jobsearch.dataaccess.model.jobs;

import java.time.Instant;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Jobs {
	private int jobId;
	private String jobTitle;
	private String jobDescription;
	private String pointsToNote;
	private String access;
	private String termsAndConditions;
	private int orgId;
	private int branchId;
	private float billingRatePerHour;
	private float fixedRate;
	private int currencyId;
	private int jobTypeId;
	private int prefectureId;
	private Instant startTime;
	private Instant endTime;
	private Instant applicationDeadline;
	private Instant cancelDeadline;
	private float penaltyCost;
	private String status;
	private Instant createdDate;
	private int createdBy;
	private int lastUpdatedBy;
	private Instant lastUpdatedDate;
	private boolean favorite;
	private List<String> jobImagesList;
	
}
