package com.axr.jobsearch.dataaccess.model.users;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthToken {

	private String accessToken;

	private String refreshToken;
	
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public void setToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	@JsonCreator
	public static AuthToken Create(String jsonString)
	{

	    AuthToken authToken = null;
	    try {
	    	ObjectMapper mapper = new ObjectMapper();
	        authToken = mapper.readValue(jsonString, AuthToken.class);
	    } catch (JsonParseException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (JsonMappingException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }

	    return authToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
}
