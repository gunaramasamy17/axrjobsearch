package com.axr.jobsearch.dataaccess.users.valueobjects;


import java.util.Date;

public class UserProfileVO {
	
	private String firstName;
	private String lastName;
	private String kanaFirstName;
	private String kanaLastName;
	private Date dateOfBirth;
	private String mobileNumber;
	private String emailAddress;
	private String password;
	private String gender;
	private int prefectureId;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getKanaFirstName() {
		return kanaFirstName;
	}
	public void setKanaFirstName(String kanaFirstName) {
		this.kanaFirstName = kanaFirstName;
	}
	public String getKanaLastName() {
		return kanaLastName;
	}
	public void setKanaLastName(String kanaLastName) {
		this.kanaLastName = kanaLastName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getPrefectureId() {
		return prefectureId;
	}
	public void setPrefectureId(int prefectureId) {
		this.prefectureId = prefectureId;
	}

}
