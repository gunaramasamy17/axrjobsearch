package com.axr.jobsearch.dataaccess.users.valueobjects;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import com.axr.jobsearch.dataaccess.model.jobs.Benefits;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobsVO {

	private int jobId;
	private String orgName;
	private String jobTitle;
	private String prefectureName;
	private Instant startTime;
	private Instant endTime;
	private float fixedRate;
	private String jobDescription;
	private String pointsToNote;
	private String termsAndConditions;
	private String orgDescription;
	private String addressDetails;
	private String contactDetails;
	private float penaltyCost;
	private String googleMapUrl;
	private float rating;
	private Instant applicationDeadline;
	private List<String> jobPrerequisiteDocs;
	private List<Benefits> benefitsList;
        private String jobImageUri;

}
