package com.axr.jobsearch.dataaccess.model.jobs;

import java.time.Instant;
import java.util.Date;

import com.axr.jobsearch.dataaccess.model.users.UserProfile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Favorites {

	private int favoriteId;
	private int jobId;
	private int userId;
	private int orgId;
	private String isActive;
	private Instant createdDate;
	private Instant lastUpdatedDate;
}
