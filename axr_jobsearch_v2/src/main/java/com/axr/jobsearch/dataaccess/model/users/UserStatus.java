package com.axr.jobsearch.dataaccess.model.users;

public enum UserStatus {
		PENDING("Pending"), 
		ACTIVE("Active"), 
		INACTIVE("Inactive");
	
		private final String value;
		
		public String value() {
			return this.value;
		}
		
		UserStatus(String value){
			this.value = value;
		}

}
