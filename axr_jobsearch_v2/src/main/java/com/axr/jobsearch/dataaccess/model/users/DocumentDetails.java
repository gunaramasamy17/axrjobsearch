package com.axr.jobsearch.dataaccess.model.users;

import java.time.Instant;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentDetails {

	private int docId;
	private String docName;
	private String docPath;
	private String docType;
	private int docCategoryId;
	private String remarks;
	private int userId;
	private Integer orgId;
	private Integer jobId;
	private String docUrl;

}
