package com.axr.jobsearch.dataaccess.email;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.axr.jobsearch.dataaccess.model.email.EmailAttribute;
import com.axr.jobsearch.dataaccess.model.email.EmailMessage;
import com.axr.jobsearch.dataaccess.model.email.EmailTemplate;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;

@Mapper
public interface EmailDataMapper {



	@Insert("insert into email_message (template_id,email_from,email_to,email_cc,email_bcc,subject,content_type,charset,status,retry_count,remarks,email_reply_to,created_date,last_updated_date) "
			+ "values(#{templateId},#{emailFrom},#{emailTo},#{emailCC},#{emailBcc},#{subject},#{contentType},#{charset},#{status},#{retryCount},#{remarks},#{emailReplyTo},#{createdDate},#{lastUpdatedDate} )")
	@Options(useGeneratedKeys = true, keyProperty = "messageId", keyColumn="message_id")
	int saveEmailMessage(EmailMessage emailMessage);
	

	@Insert("insert into email_attribute (message_id,attribute_name,attribute_value) "
			+ "values(#{messageId},#{attributeName},#{attributeValue})")
	@Options(useGeneratedKeys = true, keyProperty = "attributeId", keyColumn="attribute_id")
	int saveEmailAttribute(EmailAttribute emailAttribute);
	
	@Select("select * from email_template where template_name=#{templateName}")
	EmailTemplate getEmailTemplate(String templateName);
	
	@Update("update email_message set status=#{status} where message_id=#{messageId}")
	int updateEmailStatus(int messageId,String status);
	

}
