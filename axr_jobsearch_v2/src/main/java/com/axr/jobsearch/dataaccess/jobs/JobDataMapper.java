package com.axr.jobsearch.dataaccess.jobs;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.axr.jobsearch.dataaccess.model.jobs.Benefits;
import com.axr.jobsearch.dataaccess.model.jobs.Favorites;
import com.axr.jobsearch.dataaccess.model.jobs.JobFeedback;
import com.axr.jobsearch.dataaccess.model.jobs.Jobs;
import com.axr.jobsearch.dataaccess.model.users.DocumentDetails;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.model.users.ValidationCodeDetails;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobHistoryVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.JobsVO;
import com.axr.jobsearch.dataaccess.users.valueobjects.RatingVO;
import com.axr.jobsearch.dataaccess.utils.AxrSqlProvider;
import com.axr.jobsearch.utils.DynamicFilter;
import com.axr.jobsearch.utils.JobFilterCriteria;

@Mapper
public interface JobDataMapper {

	@Insert("insert into favorites(job_id,user_id,is_active,created_date,last_updated_date) values(#{jobId},#{userId},#{isActive},#{createdDate},#{lastUpdatedDate})")
	void addFavorites(Favorites favorite);

	//TODO: Removal at the org level or job level?
	@Update("update favorites set is_active='N', last_updated_date=#{lastUpdatedDate} where user_id=#{userId} and job_id=#{jobId}")
	void removeFavorites(int userId, int jobId, Instant lastUpdatedDate);

	
	//TODO: There is a room for optimization. Performance test required with large data set & tuning required
	/*
	@Select("<script>"
				+ "SELECT * FROM job_details "
				+ "WHERE status='A' "
				+ "AND DATE(start_time) = #{date} "
				+ "<if test='jobTypeList != null'>"
						+ "AND job_type_id IN "
							+ "<foreach item='item' index='index' collection='jobTypeList' open='(' separator=',' close=')'>"
									+ " #{item}"
							+ "</foreach> "
				+ "</if>"
				+ "<if test='prefectureList != null'>"
					+ "AND prefecture_id IN "
						+ "<foreach item='item' index='index' collection='prefectureList' open='(' separator=',' close=')'>"
								+ " #{item}"
						+ "</foreach> "
				+ "</if>"		
				+ "<if test='benefitsList != null'>"
					+ "AND job_id IN (SELECT job_id from job_benefits WHERE benefit_id IN "
						+ "<foreach item='item' index='index' collection='benefitsList' open='(' separator=',' close=')'>"
								+ " #{item}"
						+ "</foreach>) "
				+ "</if>"					
				+ "</script>")
	List<Jobs> getJobsByFilter(String date, @Param("jobTypeList") List<Integer> jobTypeList,@Param("prefectureList") List<Integer> prefectureList,@Param("benefitsList") List<Integer> benefitsList);
	*/
	
	/*
	@Select("<script>"
			+ "SELECT * FROM job_details "
			+ "WHERE status='A' "
			+ "AND DATE(start_time) = #{date} "
			+ "<if test='jobTypeList != null'>"
					+ "AND job_type_id IN "
						+ "<foreach item='item' index='index' collection='jobTypeList' open='(' separator=',' close=')'>"
								+ " #{item}"
						+ "</foreach> "
			+ "</if>"
			+ "<if test='prefectureList != null'>"
				+ "AND prefecture_id IN "
					+ "<foreach item='item' index='index' collection='prefectureList' open='(' separator=',' close=')'>"
							+ " #{item}"
					+ "</foreach> "
			+ "</if>"		
			+ "<if test='benefitsList != null'>"
				+ "AND job_id IN (SELECT job_id from job_benefits WHERE benefit_id IN "
					+ "<foreach item='item' index='index' collection='benefitsList' open='(' separator=',' close=')'>"
							+ " #{item}"
					+ "</foreach>) "
			+ "</if>"		
			+ "<if test='jobTimeMap != null'>"
			+ " AND "
			+"<foreach collection='jobTimeMap' index='key' item='jobTime' separator=' OR '>"
			    +"<foreach item='filter' collection='jobTime' separator=' AND ' open='(' close=')'> "
			          +" TIME(#{filter.columnName}) "
			          	+ "<choose>"
			          		+ "<when test=\"#{filter.operator} == 'lt'\"> < </when>"
			          		+ "<when test=\"#{filter.operator} == 'gt'\"> > </when>"
			          		+ "<when test=\"#{filter.operator} == 'le'\"> <= </when>"
			          		+ "<when test=\"#{filter.operator} == 'ge'\"> >= </when>"
			          		+ "<otherwise> = </otherwise>"
			          	+ "</choose>"
			          + " CONVERT(#{filter.value},TIME) "     
			    +"</foreach>"    
			+"</foreach>"
		+ "</if>"
			+ "</script>")
	List<Jobs> getJobsByFilter(String date, @Param("jobTypeList") List<Integer> jobTypeList,@Param("prefectureList") List<Integer> prefectureList,@Param("benefitsList") List<Integer> benefitsList, @Param("jobTimeMap") HashMap<String,List<DynamicFilter>> jobTimeMap);
*/

	//@Select("select * from job_details")
	@Results(id = "jobList", value = {
			@Result(property = "jobId", column = "job_id"),
			@Result(property = "jobTitle", column = "job_title"),
			@Result(property = "jobDescription", column = "job_description"),
			@Result(property = "pointsToNote", column = "points_to_note"),
			@Result(property = "access", column = "access"),
			@Result(property = "termsAndConditions", column = "terms_and_conditions"),
			@Result(property = "orgId", column = "org_id"),
			@Result(property = "branchId", column = "branch_id"),
			@Result(property = "billingRatePerHour", column = "billing_rate_per_hour"),
			@Result(property = "fixedRate", column = "fixed_rate"),
			@Result(property = "currencyId", column = "currency_id"),
			@Result(property = "jobTypeId", column = "job_type_id")
	})
	@SelectProvider(type=AxrSqlProvider.class, method="jobsFilterSqlStatement")
	List<Jobs> getJobs(JobFilterCriteria jobFilterCriteria);
	//List<Jobs> getJobs(String date, List<Integer> jobTypeList, List<Integer> prefectureList, List<Integer> benefitsList, List<String> filterByTime,List<String>  filterBySalaryRange, int sortBy);
	
	/*
	@Select("SELECT * FROM job_details WHERE job_id IN ( SELECT job_id FROM favorites where user_id=#[userId})")
	List<Jobs> getFavoriteJobsByFilter(int userId);
	
	@Select("SELECT * FROM job_details WHERE job_id IN ( SELECT job_id FROM job_applications where user_id=#[userId})")
	List<Jobs> getAppliedJobsByFilter(int userId);
	*/
	
	@Insert("INSERT INTO job_log_time(job_id,user_id,log_type,log_time,created_date) values(#{jobId},#{userId},#{logType},#{logTime},#{createdDate})")
	void addJobLogTime(int userId, int jobId, String logType,Instant logTime,Instant createdDate);

	//@Select("SELECT jd.job_id,org_name,jd.job_title,pre.prefecture_name,jd.start_time,jd.fixed_rate,jd.job_description,jd.points_to_note,jd.terms_and_conditions,org.org_description, org.address_details, org.contact_details,jd.penalty_cost,org.google_map_url, jd.application_deadline from organization org, job_details jd, prefecture pre, job_applications ja where jd.job_id=#{jobId} and ja.user_id = #{userId} and org.org_id=jd.org_id and jd.prefecture_id = pre.prefecture_id and ja.job_id = jd.job_id")
	@Select("SELECT jd.job_id,org_name,jd.job_title,pre.prefecture_name,jd.start_time,jd.end_time,jd.fixed_rate,jd.job_description,jd.points_to_note,jd.terms_and_conditions,org.org_description, org.address_details, org.contact_details,jd.penalty_cost,org.google_map_url, jd.application_deadline from organization org, job_details jd, prefecture pre where jd.job_id=#{jobId} and org.org_id=jd.org_id and jd.prefecture_id = pre.prefecture_id ")
	JobsVO getJobsById(int jobId, int userId);
	
	@Insert("INSERT INTO job_applications(job_id,user_id,status,penalty_applied,remarks,created_by,created_date,last_updated_by,last_updated_date) values(#{jobId},#{userId},#{status},#{penalty},#{remarks},#{userId},#{currentTime},#{userId},#{currentTime})")
	void addJobApplication(int jobId, int userId, String status, int penalty, int remarks, Instant currentTime);
	
	@Select("SELECT count(*) FROM job_applications where job_id=#{jobId} and user_id=#{userId}")
	int getJobApplicationById(int jobId, int userId);
	
	@Update("UPDATE job_applications set status =#{status}, penalty_applied=#{penalty},remarks=#{remarks},last_updated_by=#{userId},last_updated_date=#{currentTime} where user_id = #{userId} and job_id = #{jobId}")
	int updateJobApplication(int jobId, int userId, String status, int penalty,int remarks, Instant currentTime);
	
	@Select("SELECT * FROM job_feedback WHERE job_id IN ( SELECT job_id FROM job_applications  WHERE user_id=#{userId} AND status='Completed')")
	List<JobFeedback> getJobFeedback(int userId);
	
	@Select("select ja.job_id from job_applications ja,job_details jd where jd.org_id=#{orgId} and jd.job_id = ja.job_id and ja.user_id=#{userId} and ja.status='Approved' and jd.status='A' and DATE(jd.start_time) <= CURDATE() and DATE(jd.end_time) >= CURDATE() ")
	Integer getJobIdForUserByOrgId(int userId, int orgId, Date curDate);
	
	@Select("select count(*) from job_log_time where user_id = #{userId} and job_id=#{jobId} and log_type=#{logType}")
	int getJobLogRecordCount(int userId, int jobId, String logType);
	
//	@Select("SELECT b.benefit_id,b.benefit_name,b.doc_id,dd.doc_url FROM benefits b,job_benefits jb,document_details dd WHERE jb.benefit_id=b.benefit_id and dd.job_id=jb.job_id and b.doc_id = dd.doc_id and dd.doc_category_id=9")
	@Select("SELECT b.benefit_id,b.benefit_name,b.doc_id FROM benefits b ")
	List<Benefits> getBenefits();
	
	@Select("SELECT b.benefit_id, b.benefit_name,  dd.doc_url FROM benefits b,job_benefits jb,document_details dd WHERE jb.job_id=#{jobId} and jb.benefit_id=b.benefit_id and dd.job_id=jb.job_id and b.doc_id = dd.doc_id and dd.doc_category_id=8")
	List<Benefits> getBenefitsByJobId(int jobId);
		
	@Select("SELECT * FROM job_details Where job_id IN (SELECT job_id FROM job_applications where user_id=#{userId} AND status=#{status})")
	List<Jobs> getAppliedJobsByStatus(String status,int userId);
	
	//@Select("SELECT * FROM job_details where job_id IN (select job_id from job_applications where job_id in (#{jobIdList}))")
	@Select("<script>"
			+ "SELECT * FROM job_details "
			+ "WHERE job_id IN  (select job_id from job_applications where job_id in "
			           + "<if test='jobList != null'> "
				           + "<foreach item='item' index='index' collection='jobList' open='(' separator=',' close=')'>"
				              + " #{item}"
		                   + "</foreach> "
		               + "</if>"
			           +" and status in "
			           + "<if test='jobStatusList != null'> "
				           + "<foreach item='item1' index='idx' collection='jobStatusList' open='(' separator=',' close=')'>"
				              + "'#{item1}'"
		                   + "</foreach> "
	                   + "</if>"
			           + " ) </script>")
	List<Jobs> getJobsByJobStatus(@Param("jobList") List<Integer> jobIdList, @Param("jobStatusList") List<String> filterByJobStatus);
	
	@Select("select jd.job_id, jd.start_time,jd.end_time,jd.fixed_rate,jd.job_title,jf.feedback_description,ja.penalty_applied,jd.penalty_cost from job_details jd, job_feedback jf, job_applications ja where ja.user_id=#{userId} and ja.status='Approved' and ja.job_id=jd.job_id and jf.job_id=jd.job_id and jd.end_time < NOW()")
	List<JobHistoryVO> getJobHistory(int userId);
	
	@Select("SELECT AVG(rating) from job_feedback where job_id=#{jobId} and status='A' and feedback_type='END_USER'")
	Float getJobFeedbackRatingByJobId(int jobId);
	
	@Select("select dc.doc_category_name from document_category dc, job_prerequisite_docs jpd where jpd.job_id=#{jobId} and jpd.doc_category_id=dc.doc_category_id")
	List<String> getJobPrerequisiteDocCategoryNameByJobId(int jobId);
	
	@Select("select jd.org_id, sum(jf.rating) as total_rating from job_feedback jf,job_details jd where jf.job_id=jd.job_id group by jd.org_id order by total_rating desc")
	List<RatingVO> getJobFeedbackTotalRating();
	
	@Select("select * from job_details where job_id in (select job_id from favorites where user_id=#{userId})")
	List<Jobs> getFavoriteJobs(int userId);
	
	@Select("<script>"
			+ "SELECT * FROM document_details WHERE doc_category_id=8 and job_id in"
			+ "<foreach item='item' index='index' collection='jobIdList' open='(' separator=',' close=')'>"
			     + " #{item}"
			+ "</foreach> "
			+ "</script>")
	List<DocumentDetails> getJobImagesList(@Param("jobIdList") List<Integer> jobIdList);
	
	@Select("SELECT * FROM document_details WHERE job_id=#{jobId} and doc_category_id=8")
	DocumentDetails getJobImagesListByJobId(int jobId);

	@Select("SELECT application_deadline FROM job_details WHERE job_id=#{jobId}")
	Instant getApplicationDeadlineByJobId(int jobId);
}
