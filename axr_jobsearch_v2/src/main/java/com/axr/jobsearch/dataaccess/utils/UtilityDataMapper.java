package com.axr.jobsearch.dataaccess.utils;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.model.utils.ConfigParams;
import com.axr.jobsearch.dataaccess.model.utils.UserAccessToken;

@Mapper
public interface UtilityDataMapper {

	@Select("SELECT * FROM config_params")
	List<ConfigParams> getAllConfigParams();
	

	
}
