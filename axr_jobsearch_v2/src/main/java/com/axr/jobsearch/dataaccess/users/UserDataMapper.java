package com.axr.jobsearch.dataaccess.users;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.axr.jobsearch.dataaccess.model.users.DocumentDetails;
import com.axr.jobsearch.dataaccess.model.users.Languages;
import com.axr.jobsearch.dataaccess.model.users.Prefectures;
import com.axr.jobsearch.dataaccess.model.users.Privileges;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.model.users.ValidationCodeDetails;
import com.axr.jobsearch.dataaccess.model.utils.UserAccessToken;

@Mapper
public interface UserDataMapper {

	@Select("SELECT * FROM user_profile")
	List<UserProfile> getAllUsers();
	
	@Select("SELECT * FROM user_profile where UPPER(email_address) = UPPER(#{emailAddress})")
	UserProfile getUserProfile(String emailAddress);

	@Select("SELECT up.user_id,up.first_name,up.last_name,up.kana_first_name,up.kana_last_name,up.date_of_birth,up.mobile_number,up.email_address,up.password,up.gender,up.prefecture_id,up.role_id,up.prefered_language,up.user_type_id,up.is_active,up.status,up.created_date,up.last_updated_date,up.last_accessed_date FROM user_profile up where up.user_id=#{userId}")
	UserProfile getUserProfileById(int userId);
	
	@Select("SELECT up.user_id,up.first_name,up.last_name,up.kana_first_name,up.kana_last_name,up.date_of_birth,up.mobile_number,up.email_address,up.password,up.gender,up.prefecture_id,up.role_id,up.prefered_language,up.user_type_id,up.is_active,up.status,up.created_date,up.last_updated_date,up.last_accessed_date,dd.doc_url FROM user_profile up, document_details dd  where up.user_id=#{userId} and up.user_id=dd.user_id and dd.doc_category_id=5")
	UserProfile getUserProfileByIdForDocs(int userId);
	
	@Insert("insert into user_profile (first_name,last_name,kana_first_name,kana_last_name,date_of_birth,mobile_number,email_address,password,gender,prefecture_id,role_id,prefered_language,user_type_id,is_active,status,created_date,last_updated_date,last_accessed_date) "
			+ "values ( #{firstName},#{lastName},#{kanaFirstName},#{kanaLastName},#{dateOfBirth},#{mobileNumber},#{emailAddress},#{password},#{gender},#{prefectureId},#{roleId},#{preferedLanguage},#{userTypeId},#{isActive},#{status},#{createdDate},#{lastUpdatedDate},#{lastAccessedDate})")
	@Options(useGeneratedKeys = true, keyProperty = "userId", keyColumn="user_id")
	int addUserProfile(UserProfile userProfile);
	
	//TODO: Enable unique constraint on email address
	@Update("update user_profile set first_name=#{firstName},last_name=#{lastName},kana_first_name=#{kanaFirstName},kana_last_name=#{kanaLastName},date_of_birth=#{dateOfBirth},mobile_number=#{mobileNumber},password=#{password},gender=#{gender},prefecture_id=#{prefectureId},last_updated_date=#{lastUpdatedDate} where email_address = #{emailAddress} ")
	void updateUserProfile(UserProfile userProfile);

	@Insert("insert into validation_code_details(validation_category,user_id,code_info,valid_till,email_message_id,validation_status) values(#{validationCategory},#{userId},#{codeInfo},#{validTill},#{emailMessageId},#{validationStatus})")
	int addVerificationCode(ValidationCodeDetails validationCodeDetails);
	
	@Select("select * from validation_code_details where code_info=#{validationCode}")
	ValidationCodeDetails getValidationCodeDetails(String validationCode);
	
	@Update("update validation_code_details set validation_status=#{validationStatus}, validation_date=#{updateTimestamp}  where code_info=#{validationCode}")
	void updateValidationStatus(String validationStatus, String validationCode, Instant updateTimestamp);
	
	@Update("update user_profile set status=#{status},is_active=#{isActive},last_updated_date=#{updateTimestamp} where user_id=#{userId}")
	void updateUserStatus(String status, String isActive, int userId, Instant updateTimestamp);
	
	@Select("select * from prefecture where country_id=#{countryId}")
	List<Prefectures> getPrefectures(int countryId);
	
	@Update("update user_profile set password=#{password},last_updated_date=#{updateTimestamp} where user_id=#{userId}")
	void updateUserPassword(int userId, String password, Instant updateTimestamp);
	
	//TODO: From security point of view, we should not store all the token in for single table.... This needs to be revisited for production release.
	@Insert("insert into user_access_token(user_id,jti,secret_key,token,created_date,last_accessed_date) values(#{userId},#{jti},#{secretKey},#{token},#{createdDate},#{lastAccessedDate})")
	@Options(useGeneratedKeys = true, keyProperty = "tokenId", keyColumn="token_id")
	int saveUserAccessToken(UserAccessToken userAccessToken);
	//int saveUserAccessToken(int userId,String jti,String secretKey,String accessToken,String refreshToken,Instant createdDate, Date accessExpiry, Date refreshExpiry);

	@Select("select * from languages")
	List<Languages> getLanguages();
	
	@Update("update user_profile set prefered_language=#{languageId} where user_id = #{userId}")
	void updateLanguage(int userId, int languageId);
	
	@Select("SELECT * FROM user_access_token where jti = #{jti}")
	UserAccessToken getUserAccessToken(String jti);
	
	@Update("UPDATE user_access_token set last_accessed_date = #{lastAccessedDate} where jti = #{jti}")
	void updateUserTokenLastAccessedTime(UserAccessToken userAccessToken);
	
	@Select("SELECT privilege_name FROM privileges where privilege_id in (SELECT privilege_id FROM role_privilege_map WHERE role_id = #{roleId})")
	List<String> getPrivileges(int roleId);
	
	@Delete("DELETE FROM user_access_token WHERE user_id=#{userId} AND token = #{token}")
	void axrLogout(int userId,String token);
	
	@Insert("insert into document_details(doc_name,doc_path,doc_type,doc_category_id,user_id,org_id,job_id,doc_url) values(#{docName},#{docPath},#{docType},#{docCategoryId},#{userId},#{orgId},#{jobId},#{docUrl})")
	//@Options(useGeneratedKeys = true, keyProperty = "docId", keyColumn="doc_id")
	void addDocuments(String docName, String docPath, String docType, int docCategoryId, int userId, Integer orgId, Integer jobId, String docUrl);
	
	@Select("select * from document_details where doc_category_id=#{docCategoryId} and user_id = #{userId}")
	DocumentDetails getDocuments(int docCategoryId, int userId);
	
	@Select("select * from document_details where doc_id=${docId} and user_id = #{userId}")
	DocumentDetails getDocumentsByDocId(int docId, int userId);
	
	@Select("select * from document_details where user_id = #{userId} and doc_category_id=5")
	DocumentDetails getDocumentsByUserId(int userId);
	
	@Update("update document_details set doc_name=#{docName}, doc_path=#{docPath}, doc_type=#{docType}, doc_category_id=#{docCategoryId}, user_id=#{userId}, org_id=#{orgId}, job_id=#{jobId}, doc_url=#{docUrl} where doc_id=#{docId}")
	void updateDocuments(int docId, String docName, String docPath, String docType, int docCategoryId, int userId, Integer orgId, Integer jobId, String docUrl);

        


}
