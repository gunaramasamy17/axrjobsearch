package com.axr.jobsearch.dataaccess.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.utils.DynamicFilter;
import com.axr.jobsearch.utils.JobFilterCriteria;



public class AxrSqlProvider implements Serializable {
	
	private static final long serialVersionUID = -558018063689455411L;
	
	static final Logger logger = LoggerFactory.getLogger(AxrSqlProvider.class);

    public String jobsFilterSqlStatement( @Param("jobFilterCriteria") JobFilterCriteria jobFilterCriteria) {
    	    	//TODO: SQL Injection possibility?
    	    	String sql = "SELECT * FROM job_details WHERE status='A' ";
    	    	
    	    	if(null != jobFilterCriteria.getDate() && !"".equalsIgnoreCase(jobFilterCriteria.getDate())) sql+=" AND DATE(start_time) = '"+jobFilterCriteria.getDate()+"'";
    	    	
    	    	String value = "";
    	        if(null != jobFilterCriteria.getJobTypeList() && jobFilterCriteria.getJobTypeList().size()>0) {
    	        	
    	    		for(int i : jobFilterCriteria.getJobTypeList()) {
    	    			value+=i+",";
    	    		}
    	    		if(value.endsWith(",")) value=value.substring(0,value.length()-1);
    	    		
    	    		sql+=" AND job_type_id IN ( "+value+" ) ";
    	        }
    	        
    	        value = "";
    	        if(null != jobFilterCriteria.getPrefectureList() && jobFilterCriteria.getPrefectureList().size()>0) {
    	        	
    	    		for(int i : jobFilterCriteria.getPrefectureList()) {
    	    			value+=i+",";
    	    		}
    	    		if(value.endsWith(",")) value=value.substring(0,value.length()-1);
    	    		
    	    		sql+=" AND prefecture_id IN ( "+value+" ) ";
    	        }
    	        
    	        value = "";
    	        if(null != jobFilterCriteria.getBenefitsList() && jobFilterCriteria.getBenefitsList().size()>0) {
    	        	
    	    		for(int i : jobFilterCriteria.getBenefitsList()) {
    	    			value+=i+",";
    	    		}
    	    		if(value.endsWith(",")) value=value.substring(0,value.length()-1);
    	    		
    	    		sql+=" AND job_id IN (SELECT job_id from job_benefits WHERE benefit_id IN ( "+value+" ) ) ";
    	        }
    	        
    	        value = "";
    	        String[] val;
    	        if(null != jobFilterCriteria.getFilterByTime() && jobFilterCriteria.getFilterByTime().size()>0) {
    	        	sql+=" AND (";
    	        	for(int i=0; i<jobFilterCriteria.getFilterByTime().size(); i++) {
    	        		val = jobFilterCriteria.getFilterByTime().get(i).split("-");
    	        		
    	        		sql+=" ( TIME(start_time) >= CONVERT('"+val[0]+"',TIME) AND TIME(end_time) <= CONVERT('"+val[1]+"',TIME) )";
    	        		if(i+1<jobFilterCriteria.getFilterByTime().size()) sql+=" OR ";
    	    		}
    	    		sql+=" ) ";
    	    	}
    	        
    	        value = "";
    	        val=null;
    	        if(null != jobFilterCriteria.getFilterBySalaryRange() && jobFilterCriteria.getFilterBySalaryRange().size()>0) {
    	        	sql+=" AND (";
    	        	for(int i=0; i<jobFilterCriteria.getFilterBySalaryRange().size(); i++) {
    	        		val = jobFilterCriteria.getFilterBySalaryRange().get(i).split("-");        		
    	        		sql+=" ( fixed_rate >= "+val[0]+" AND fixed_rate <= "+val[1]+" ) ";
    	        		if(i+1<jobFilterCriteria.getFilterBySalaryRange().size()) sql+=" OR ";
    	    		}
    	    		sql+=" ) ";
    	    	}
    	        
    	        if(jobFilterCriteria.getList().equalsIgnoreCase(Constants.FAVORITE_JOBS)) {
    	    		sql+=" AND job_id IN (SELECT job_id from favorites WHERE user_id = "+jobFilterCriteria.getUserId()+" AND is_active='Y' ) ";
    	        }else if(jobFilterCriteria.getList().equalsIgnoreCase(Constants.APPLIED_JOBS)) {
    	    		sql+=" AND job_id IN (SELECT job_id from job_applications WHERE user_id = "+jobFilterCriteria.getUserId()+" ) ";
    	        }
    	        
    	        if(null != jobFilterCriteria.getJobStatusList() && jobFilterCriteria.getJobStatusList().size()>0) {
    	        	value="";
    	        	for(String s : jobFilterCriteria.getJobStatusList()) {
    	        		value+="'"+s+"',";
    	        	}
    	        	if(value.endsWith(",")) value=value.substring(0,value.length()-1);
    	        	
    	        	sql+=" AND job_id in (SELECT job_id from job_applications WHERE user_id ="+jobFilterCriteria.getUserId()+" AND status IN ("+value+") ) ";
    	        }
    	        
    	        
    	        if(jobFilterCriteria.getSortBy() == 1) {
    	        	//Popularity?
    	        }else if(jobFilterCriteria.getSortBy() == 2) {
    	        	sql+=" order by created_date desc";
    	        }else if(jobFilterCriteria.getSortBy() == 3) {
    	        	sql+=" order by fixed_rate asc";
    	        }else if(jobFilterCriteria.getSortBy() == 4) {
    	        	sql+=" order by fixed_rate desc";
    	        }
    	        
    	        logger.error("\n\n===============================================\n"+sql+"\n\n");
    	    	return sql;
    	    }

}
