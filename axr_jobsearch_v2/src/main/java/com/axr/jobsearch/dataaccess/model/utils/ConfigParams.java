package com.axr.jobsearch.dataaccess.model.utils;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfigParams {
	
	public int paramId;
	public String paramName;
	public String paramValue;
	public String paramCategory;
	

}
