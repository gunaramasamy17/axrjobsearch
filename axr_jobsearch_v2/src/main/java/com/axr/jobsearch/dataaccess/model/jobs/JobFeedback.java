package com.axr.jobsearch.dataaccess.model.jobs;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobFeedback {
	private int jobFeedbackId;
	private int jobId;
	private String feedbackType;
	private Float rating;
	private String feedbackDescription;
	private String status;
	private int remarks;
	private int createdBy;
	private Instant createdDate;
	private int lastUpdatedBy;
	private Instant lastUpdatedDate;
	
}
