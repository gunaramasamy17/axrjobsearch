package com.axr.jobsearch.dataaccess.model.users;

import java.time.Instant;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ValidationCodeDetails {
	
	private int validationCodeId;
	private String validationCategory;
	private int userId;
	private String codeInfo;
	private Instant validTill;
	private int emailMessageId;
	private String validationStatus;	
	private Instant validationDate;
	
	public int getValidationCodeId() {
		return validationCodeId;
	}
	public void setValidationCodeId(int validationCodeId) {
		this.validationCodeId = validationCodeId;
	}
	public String getValidationCategory() {
		return validationCategory;
	}
	public void setValidationCategory(String validationCategory) {
		this.validationCategory = validationCategory;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getCodeInfo() {
		return codeInfo;
	}
	public void setCodeInfo(String codeInfo) {
		this.codeInfo = codeInfo;
	}
	public Instant getValidTill() {
		return validTill;
	}
	public void setValidTill(Instant validTill) {
		this.validTill = validTill;
	}
	public int getEmailMessageId() {
		return emailMessageId;
	}
	public void setEmailMessageId(int emailMessageId) {
		this.emailMessageId = emailMessageId;
	}
	public String getValidationStatus() {
		return validationStatus;
	}
	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}
	public Instant getValidationDate() {
		return validationDate;
	}
	public void setValidationDate(Instant validationDate) {
		this.validationDate = validationDate;
	}
	
	
}
