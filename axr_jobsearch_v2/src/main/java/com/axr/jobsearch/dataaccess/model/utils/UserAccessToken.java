package com.axr.jobsearch.dataaccess.model.utils;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAccessToken {
	
	private long tokenId;
	private int userId;
	private String jti;
	private String secretKey;
	private String token;
	private Instant createdDate;
	private Instant lastAccessedDate;

}
