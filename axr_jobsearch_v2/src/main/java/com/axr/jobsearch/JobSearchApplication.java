package com.axr.jobsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Bean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.web.client.RestTemplate;

import com.axr.jobsearch.utils.ConfigMap;
import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
@ComponentScan({"com.axr.*"})
@MapperScan(basePackages = {"com.axr.jobsearch.dataaccess","com.axr.jobsearch.dataaccess.*","com.axr.jobsearch.dataaccess.users","com.axr.jobsearch.dataaccess.users.*"})
public class JobSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobSearchApplication.class, args);
		//ConfigMap.getInstance();
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	/**
	 * @return - Docket
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).host("34.69.194.5").apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.any())
				.paths(Predicates.not(PathSelectors.regex("/error")))
				.paths(PathSelectors.ant("/**")).build();
	}

	/**
	 * @return - ApiInfoBuilder
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("The AXR Job Search Platform API")
				.description(
						"The AXR Job Search Platform lets the users to connect with suitable jobs posted by the business people in nearby locations.")
				.license("AXR License").licenseUrl("http://www.axr.com/license.html").version("0.1").build();
	}
	

}
