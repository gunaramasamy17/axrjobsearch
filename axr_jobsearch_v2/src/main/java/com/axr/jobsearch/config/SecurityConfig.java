package com.axr.jobsearch.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.axr.jobsearch.security.AuthEntryPoint;
import com.axr.jobsearch.security.AuthFailureHandler;
import com.axr.jobsearch.security.AuthProvider;	
import com.axr.jobsearch.security.AuthSuccessHandler;
import com.axr.jobsearch.security.AuthenticationTokenFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AuthEntryPoint unauthorizedHandler;
	
	@Autowired
	private AuthProvider authProvider;
	
	@Bean
	@Override
	public AuthenticationManager authenticationManager() throws Exception {
		return new ProviderManager(Arrays.asList(authProvider));
	}

	public AuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
		AuthenticationTokenFilter authenticationTokenFilter = new AuthenticationTokenFilter();
		authenticationTokenFilter.setAuthenticationManager(authenticationManager());
		authenticationTokenFilter.setAuthenticationSuccessHandler(new AuthSuccessHandler());
		authenticationTokenFilter.setAuthenticationFailureHandler(new AuthFailureHandler());
		return authenticationTokenFilter;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		  http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().
		  exceptionHandling() .authenticationEntryPoint(unauthorizedHandler).and()
		  .addFilterBefore(authenticationTokenFilterBean(),
		  UsernamePasswordAuthenticationFilter.class)
		  .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		 
		http.headers().cacheControl();
		
	}
	
	
	@Override
	public void configure(WebSecurity webSecurity) throws Exception
	{
		//TODO: cleanup required for appropriate path
		webSecurity
		.ignoring()
		.antMatchers("/swagger-ui.html")
		.antMatchers("/swagger-resources/**")
		.antMatchers("/v2/api-docs/**")
		.antMatchers("/webjars/**")
		.antMatchers("/login/**")
		.antMatchers("/error/**")
		.antMatchers("/forgotPassword/**")
		.antMatchers("/resetPassword/**")
		.antMatchers(HttpMethod.POST,"/users/**")
		.antMatchers("/activate/**")
		.antMatchers("/prefectures/**");
		
		
	}
	
	
	
	
}
