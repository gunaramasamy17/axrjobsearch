package com.axr.jobsearch.common.validator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axr.jobsearch.dataaccess.model.users.Privileges;
import com.axr.jobsearch.dataaccess.users.UserDataMapper;

@Component
public class PrivilegeProvider {
	
	static final Logger logger = LoggerFactory.getLogger(PrivilegeProvider.class);

	@Autowired
	UserDataMapper userDataMapper;
	
	public List<String> getPrivileges(int roleId) {
		return userDataMapper.getPrivileges(roleId);
	}
}
 