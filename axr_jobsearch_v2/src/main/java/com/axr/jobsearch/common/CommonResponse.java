package com.axr.jobsearch.common;

public class CommonResponse<T> {
	private String statusCode;
	private String message;
	private T data;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NL = System.getProperty("line.separator");
		result.append("statusCode: "+statusCode+NL);
		result.append("message: "+message+NL);
		if(null != data) result.append("data: "+data+NL);
		return result.toString();
	}

}
