package com.axr.jobsearch.common.exception;

public class ExceptionUtils {
	
	public static String getErrorResponseBody(String errorCode ,String errorMessage){
		return "{ \"errorCode\": \"" + errorCode + "\" "
        		+ ",\"errorMessage\": \"" + errorMessage + "\"}";
	}

}
