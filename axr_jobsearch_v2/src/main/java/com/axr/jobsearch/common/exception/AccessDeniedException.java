package com.axr.jobsearch.common.exception;

public class AccessDeniedException {
	
	//TODO: Custom Exceptions can be handled differently.
	
	private Integer statusCode = Integer.valueOf(0);
	private String statusMessage = "";
	
	public AccessDeniedException(Integer statusCode, String statusMessage) {
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("\n Status Code: " + this.statusCode);
		builder.append("\n Status Message: " + this.statusMessage);
	
		return builder.toString();
		
	}
}
