package com.axr.jobsearch.common.exception;

import org.springframework.security.core.AuthenticationException;

public class LoginAuthenticationException extends AuthenticationException {
	
	//TODO: Custom Exceptions can be handled differently.
	private int statusCode = 0;
	private String statusMessage = "";
	private Exception exception = null;
	private String errorCode;
	
	
	public LoginAuthenticationException(String statusMessage, Exception exception) {
		super(statusMessage,exception);
		//this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.exception = exception;
		this.errorCode = statusMessage;

	}
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("\n Status Code: " + this.statusCode);
		builder.append("\n Status Message: " + this.statusMessage);
	
		return builder.toString();
		
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


}
