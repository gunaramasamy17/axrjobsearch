package com.axr.jobsearch.common.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.utils.ConfigMap;

public class UserValidator {

	public static ArrayList<String> validateUser(String password, String emailAddress, Date dateOfBirth,
			String firstName, String lastName, String kanaFirstName, String kanaLastName, String mobileNumber ) {
		ArrayList<String> errorList = new ArrayList<String>();

		// TODO: Validations for email format, password complexity, dob etc...
		if (null != password
				&& !password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
			errorList.add("強力なパスワードを入力してください (最小8、1つの大文字、1つの小文字、1つの数値、1つの特殊文字)");

		}

		if (null != emailAddress && !emailAddress.matches("^[A-Za-z0-9+_.-]+@(.+)$")) {
			errorList.add("妥当なメールアドレスを入力してください");

		}
 
		if (null != firstName && !firstName.matches("^[ァ-・ヽヾ゛゜ー]{1,}$")) {
			if (!firstName.matches("^[一-龯]{1,}$")) {
				errorList.add("名は漢字あるいはかなで入力してください");
			}
		}

		if (null != lastName && !lastName.matches("^[ァ-・ヽヾ゛゜ー]{1,}$")) {
			if (!lastName.matches("^[一-龯]{1,}$")) {
				errorList.add("苗字は漢字あるいはかなで入力してください");
			}
		}
		if (null != kanaFirstName && !kanaFirstName.matches("^[ァ-・ヽヾ゛゜ー]{1,}$")) {
			errorList.add("かなで入力してください");
		}
		if (null != kanaLastName && !kanaLastName.matches("^[ァ-・ヽヾ゛゜ー]{1,}$")) {
			errorList.add("かなで入力してください");
		}
		if (null != mobileNumber && !mobileNumber.matches("^[0][0-9]{10}$")) {
			errorList.add("妥当な電話番号を入力してください");
		}
		if (null != dateOfBirth && dateOfBirth.after(new java.util.Date())) {
			errorList.add("生年月日は16歳以上である必要があります");
		} else if (null != dateOfBirth) {
			long age = (System.currentTimeMillis() - dateOfBirth.getTime()) / (364 * 24 * 60 * 60 * 1000l);
			if (age < Integer.parseInt(ConfigMap.getInstance().getConfigCategoryMap().get(Constants.USER_CONFIG)
					.get(Constants.USER_MIN_AGE))) {
				errorList.add("生年月日は16歳以上である必要があります");
			}
		}
		return errorList;
	}
}
