package com.axr.jobsearch.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.axr.jobsearch.service.UtilityService;
import com.axr.jobsearch.utils.ConfigMap;
import com.axr.jobsearch.utils.EmailServer;


@Component
@Order(0)
class MyApplicationListener implements ApplicationListener<ApplicationReadyEvent> {

	static final Logger logger = LoggerFactory.getLogger(MyApplicationListener.class);
	
	@Autowired
	UtilityService utilityService;

  @Override
  public void onApplicationEvent(ApplicationReadyEvent event) {
	  try {
		  	ConfigMap.getInstance().setConfigCategoryMap(utilityService.getConfigParameters());
		  	//EmailServer.getInstance().getSession();
	  }catch(Exception ex) {
		  logger.error("Error while loading",ex);
	  }
  }

}
 