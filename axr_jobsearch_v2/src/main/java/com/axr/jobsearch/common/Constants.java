package com.axr.jobsearch.common;

public class Constants {
	
	
	public final static String STATUS_YES = "Y";
	public final static String STATUS_NO = "N";
	
	public final static String PENDING = "PENDING";
	public final static String SUCCESS = "SUCCESS";
	public final static String ACTIVE = "ACTIVE";
	
	
	public final static String VALIDATION_CATEGORY_NEW_USER = "NEW_USER_ACTIVATION";
	public final static String VALIDATION_CATEGORY_FORGOT_PASSWORD = "FORGOT_PASSWORD";
	
	public final static String USER_STATUS_PENDING = "PENDING_ACTIVATION";
	public final static String USER_STATUS_DELETED = "DELETED";
	
	//TODO: Use DB config param
	public final static int DEFAULT_USER_ACTIVATION_CODE_VALIDITY_IN_HOURS = 24;
	public final static int DEFAULT_USER_FORGOT_PASSWORD_CODE_VALIDITY_IN_HOURS = 2;
	//Default user details
	//TODO: Change the implementation INSTEAD OF HARDCODING
	public final static int DEFAULT_USER_ROLE_ID = 4;
	public final static int DEFAULT_USER_TYPE_ID = 3;
	public final static int DEFAULT_USER_LANGUAGE_ID = 2;
	
	//Email related
	public final static String EMAIL = "Email";
	public final static String INVITE_USER_TEMPLATE = "INVITE_USER";
	public final static String FORGOT_PASSWORD_TEMPLATE = "FORGOT_PASSWORD";
	public final static String FIRST_NAME = "FIRST_NAME";
	public final static String LAST_NAME = "LAST_NAME";
	public final static String ACTIVATION_LINK = "ACTIVATION_LINK";
	public final static String RESET_PASSWORD_LINK = "RESET_PASSWORD_LINK";
	public final static String EMAIL_FROM = "mail.smtp.user.name";
	
	public final static String TOKEN="TOKEN";
	public final static String ACCESS_TOKEN_EXPIRY="expiry.access.token";
	public final static String REFRESH_TOKEN_EXPIRY="expiry.refresh.token";
	
	public final static String UTF8 = "UTF-8";
	public final static String JTI = "jti";
	public final static String AUTHORIZATION = "Authorization";
	public final static String BEARER = "Bearer ";
	
	public final static String OPEN_JOBS="openJobs";
	public final static String FAVORITE_JOBS="favoriteJobs";
	public final static String APPLIED_JOBS="appliedJobs";
	
	public final static String JOB_APPLIED="Applied";
	public final static String JOB_APPROVED="Approved";
	public final static String JOB_DENIED="Denied";
	public final static String JOB_CANCEL="Cancel";
	public final static String JOB_COMPLETED="Completed";
	public final static String JOB_CANCEL_BY_USER="Cancel By User";
	public final static String JOB_CANCEL_BY_ORG="Cancel By Org";
	
	public final static String USER_CONFIG="User";
	public final static String USER_MIN_AGE="user.min.age";
	
	public final static String DOCUMENTS = "Documents";
	public final static String FILE_STORE_PATH = "document.store.path";
	public final static String DOT = ".";
	
	public final static String ASSETS="/assets/";

	

}
