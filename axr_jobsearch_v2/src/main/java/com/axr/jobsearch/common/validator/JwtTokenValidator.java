package com.axr.jobsearch.common.validator;

import java.time.Instant;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.axr.jobsearch.common.Constants;
import com.axr.jobsearch.common.exception.LoginAuthenticationException;
import com.axr.jobsearch.dataaccess.model.users.UserProfile;
import com.axr.jobsearch.dataaccess.model.utils.UserAccessToken;
import com.axr.jobsearch.dataaccess.users.UserDataMapper;
import com.axr.jobsearch.dataaccess.utils.UtilityDataMapper;
import com.axr.jobsearch.security.AuthUserDetails;
import com.axr.jobsearch.service.UserService;
import com.axr.jobsearch.utils.JwtTokenUtils;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

@Component
public class JwtTokenValidator {
	
	static final Logger logger = LoggerFactory.getLogger(JwtTokenValidator.class);

	@Autowired
	UserDataMapper userDataMapper;
	
	@Autowired
	JwtTokenUtils jwtTokenUtils;
	
	public AuthUserDetails parseToken(String token) {
		
		AuthUserDetails authUserDetails = null;
		
		String secretKey = null;
		String jti = null;
		
		JSONObject payload = jwtTokenUtils.getPayload(token);
		logger.info("payload::"+payload);
		
		try {
			jti = payload.get(Constants.JTI).toString();
		}catch(JSONException e) {
			logger.error("Error while retriving JTI from token",e);
		}
		
		
		
		
		try {
			
			UserAccessToken userAccessToken = userDataMapper.getUserAccessToken(jti);
			
			if(null!=userAccessToken) {
				userAccessToken.setLastAccessedDate(Instant.now());
				userDataMapper.updateUserTokenLastAccessedTime(userAccessToken);
				secretKey = userAccessToken.getSecretKey();
			}else {
				throw new LoginAuthenticationException("Token validation error:",null);
			}
			
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
			UserProfile userProfile = userDataMapper.getUserProfileById(userAccessToken.getUserId());
			authUserDetails = new AuthUserDetails();
			authUserDetails.setUserId(userProfile.getUserId());
			authUserDetails.setUserTypeId(userProfile.getUserTypeId());
			authUserDetails.setEmailAddress(userProfile.getEmailAddress());
			authUserDetails.setActiveFrom(userProfile.getCreatedDate());
			authUserDetails.setFirstName(userProfile.getFirstName());
			authUserDetails.setLastName(userProfile.getLastName());
			authUserDetails.setPrefectureId(userProfile.getPrefectureId());
			authUserDetails.setPreferedLanguage(userProfile.getPreferedLanguage());
			authUserDetails.setMobileNumber(userProfile.getMobileNumber());
			authUserDetails.setRoleId(userProfile.getRoleId());
			authUserDetails.setStatus(userProfile.getStatus());
			authUserDetails.setToken(userAccessToken.getToken());
			authUserDetails.setLastAccessed(userAccessToken.getLastAccessedDate());			
			
		}catch(LoginAuthenticationException e) {
			throw new LoginAuthenticationException("Token validation error:",e);
		}catch(JwtException | JSONException e) {
			logger.error("Error while signature validation of token->"+token+"<-", e);
			throw new LoginAuthenticationException("Token validation error",e);
		}catch(Exception ex) {
			logger.error("Error while validation of token->"+token+"<-", ex);
			throw new LoginAuthenticationException("Token validation error:",ex);
		}
		
		return authUserDetails;
	}
	
	
}
