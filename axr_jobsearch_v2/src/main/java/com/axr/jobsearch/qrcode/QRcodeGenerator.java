package com.axr.jobsearch.qrcode;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import java.util.Hashtable;
import com.google.zxing.EncodeHintType;
import com.axr.jobsearch.dataaccess.model.organization.Organization;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QRcodeGenerator {


	public String writeQRCode(Organization organizationInfo) throws IOException, WriterException {

		Hashtable hints = new Hashtable();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		QRCodeWriter writer = new QRCodeWriter();
		BitMatrix bitMatrix = writer.encode(organizationInfo.getOrgId()+ "\n" +
				organizationInfo.getOrgName() + "\n" + organizationInfo.getOrgDescription() + "\n"
						+ organizationInfo.getOrgCategory() + "\n" + organizationInfo.getPrefectureId() + "\n"
						+ organizationInfo.getStatus() + "\n" + organizationInfo.getIsActive(),
				BarcodeFormat.QR_CODE, 300, 300,hints);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
		String encodedData = Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
          
	
		
		
//	 	byte[] decodedData = Base64.getDecoder().decode(encodedData);
//		int codeId = organizationInfo.getOrgId();
//		ByteArrayInputStream input = new ByteArrayInputStream(decodedData);
//		BufferedImage image = ImageIO.read(input);
//		ImageIO.write(image, "png", new File(codeId + "Image.png"));

		return encodedData;
	}
}

